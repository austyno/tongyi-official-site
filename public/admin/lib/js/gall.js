/* =====================================
All JavaScript functions Start
======================================*/

(function ($) {

  'use strict';
  /*--------------------------------------------------------------------------------------------
    document.ready ALL FUNCTION START
  ---------------------------------------------------------------------------------------------*/


  //________magnificPopup function	by = magnific-popup.js________//	

  function magnific_popup() {
    jQuery('.mfp-gallery').magnificPopup({
      delegate: '.mfp-link',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
      }
    });
  }


  //________ magnificPopup for video function	by = magnific-popup.js________//	

  function magnific_video() {
    jQuery('.mfp-video').magnificPopup({
      type: 'iframe',
    });
  }



  jQuery(document).ready(function () {

    //________magnificPopup function	by = magnific-popup.js________//	
    magnific_popup(),
      //________magnificPopup for video function	by = magnific-popup.js________//	
      magnific_video()

  });


})(window.jQuery);