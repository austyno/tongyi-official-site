    $(document).ready(function () {
        $(document).on('click', '#del', function (e) {

            // alert('clicked')
            e.preventDefault();

            var id = $(this).data('id');
            var title = $(this).data('title');


            // $('#myModalLabel').html(id);
            $('#title').html(title);
            $('#hidden').val(id)
            $('#myModal').modal('show')
        })

        $(document).on('click', '#yes', function () {

            var id = $('#hidden').val();
            var token = "{{csrf_token()}}"
            var url = '/delete'

            $('#myModal').modal('hide')


            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'html',
                data: {
                    id: id,
                    _token: token
                }

            })
                .done(function (data) {
                    data = JSON.parse(data)

                    if (data == 'successful') {
                        window.location = "{{route('news')}}"
                    }

                })

        });

        $(document).on('click', '#remove', function () {

            var id = $(this).data('id');

            $('.bs-example-modal-sm').modal('show')

            $('#imgId').val(id)
            $('#imgNo').html(id)



            $(document).on('click', '#cnt', function () {

                var token = "{{csrf_token()}}"
                var imgNo = $('#imgId').val()
                var url = '/deleteImg'

                $('.bs-example-modal-sm').modal('hide')


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'html',
                    data: {
                        id: id,
                        _token: token
                    }
                })
                    .done(function (data) {
                        data = JSON.parse(data)
                        console.log(data)

                        if (data == 'successful') {
                            window.location = "{{ url()->previous() }}"
                        }

                    })


            })

        })


        $(document).on('click', '#letter', function () {

            var id = $(this).data('id');
            var url = "/letter";
            var token = "{{csrf_token()}}"

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'html',
                data: {
                    id: id,
                    _token: token
                }

            }).done(function (data) {
                data = JSON.parse(data)

                $('#content').html(data.coverLetter)
                $('#name').html(data.name)
            })
        })

    })