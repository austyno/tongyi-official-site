<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Storage;
use Response;
use App\Vacancy;
use App\Application;

class VacancyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allJobs = Vacancy::all();

        return view('admin.pages.jobs.index')->with([
            'allJobs' => $allJobs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.jobs.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Vacancy::create([
            'title'         => $request->title,
            'slots'         => $request->slots,
            'department'    => $request->department,
            'qualification' => $request->qualification
        ]);

        Session::flash('success','job created successfully');
        return redirect()->route('jobs.all');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Response $request,$id)
    {

        $file= Application::find($id);

        $filePath = public_path($file->resume);

        return response()->file($filePath);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editJob = Vacancy::find($id);

        return view('admin.pages.jobs.edit')->with([
            'editJob'   => $editJob
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Vacancy::where([
            'id' => $id
        ])->update([
            'title' => $request->title,
            'department'    =>  $request->department,
            'qualification' =>  $request->qualification,
            'slots'         =>  $request->slots
        ]);

        Session::flash('success','job updated successfully');
        return redirect()->route('jobs.all');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delJob = Vacancy::find($id);

        $delJob->delete();

        Session::flash('success','job deleted successfully');
        return redirect()->route('jobs.all');

    }

    public function applications()
    {
        $applications = Application::all();

        return view('admin.pages.jobs.applications')->with([
            'applications'  => $applications 
        ]);


    }

    public function coverLetter(Request $request)
    {
        $id = $request->id;
        $application = Application::find($id);
        $letter = $application->coverLetter;


        return response()->json($application);
    }
    
    public function deleteApplication($id)
    {
        $application = Application::find($id);

        $application->delete();

        Session::flash('success','application deleted successfully');

        return redirect()->back();


    }


}
