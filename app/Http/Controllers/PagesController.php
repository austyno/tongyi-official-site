<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Response;
use Session;
use App\Project;
use App\ProjectImage;
use App\News;
use App\Gallery;
use App\Application;
use App\Vacancy;
use App\Enquiry;
use Cache;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::where(['published' => 'YES'])->orderBy('created_at', 'desc')->get()->take(3);

        // dd($news);
        return view('welcome')->with('news',$news);
                                
    }

    public function menuPages(Request $request, $name)
    {
        //get news items
        $news = News::where(['published' => 'YES'])->orderBy('created_at', 'desc')->paginate(9);

        // dd($news);
        // $completedProjects = Project::with('images')->where('status','completed')->get();
        $projects = Project::with('images')->get();


        //completed projects
        $completedProjects = Project::with(['images'])->where(['status' => 'completed'])->get();

        //ongoing projects
        $ongoinProjects = Project::with(['images'])->where(['status' => 'ongoing'])->get();

        //jobs
        $jobs = Vacancy::all();


        //gallery images
        $galleryImages = Gallery::paginate(9);

        //  dd($name);
        //split name slug
        $nameArray = preg_split('/\-/',$name);

        if(count($nameArray) > 1){

            $pageName = $nameArray[0].$nameArray[1];

        }else{

            $pageName = $name;
        }


        // dd($pageName);
        $crumb = preg_replace('/\-/'," ", $name);


        return view('pages/'.$pageName)->with([
            'completedProjects' => $completedProjects,
            'ongoinProjects'    => $ongoinProjects,
            'galleryImages'     => $galleryImages,
            'news'              => $news,
            'jobs'              => $jobs,
            'crumb'             => $crumb
        ]);
    }

    public function readmore($slug)
    {
        $newsItem = News::where('slug', $slug)->get();
        $news = News::where(['published' => 'YES'])->orderBy('created_at', 'desc')->get()->take(3);

        //  dd($newsItem);

        return view('pages.readmore')->with('newsItem',$newsItem)
                                    ->with('news',$news);

    }

    public function viewimages($status,$slug)
    {


        $project = Project::where(['slug' => $slug, 'status' => $status])->get();

        $images = ProjectImage::where(['project_id' => $project[0]->id])->get();

        $crumb = $project[0]->name;


        return view('pages.imageview')->with(['crumb' => $crumb,'images' => $images]);
    }

    public function getDownload()
    {

    $file= public_path(). "/download/TongyiProfile.pdf";
    $name = 'TongyiProfile.pdf';

    $headers =[
        'Content-Type' => 'application/pdf'
    ];

    return Response::download($file, $name, $headers);
    }

    public function apply(Request $request)
    {
        $request->validate([
            'surename' => 'required',
            'resume' => 'required|mimes:doc,pdf',
            'coverLetter' => 'required',
            'specialization' => 'required',
            'email' => 'required|email',
            'postalCode' => 'required|int',
            'tel' => 'required|int',
            'country' => 'required',
            'address' => 'required',
            'name' => 'required',
        ]);

            $resume = $request->resume;

            $split = preg_split('/\./',$resume->getClientOriginalName());

            // dd($split);

            $newName = $request->surename.'.'.$split[1];

            // dd($newName);

            $resume->move('resumes/', $newName);

            Application::create([
            'surename'        => $request->surename,
            'resume'          => 'resumes/'.$newName,
            'coverLetter'     => $request->coverLetter,
            'specialization'  => $request->specialization,
            'email'           => $request->email,
            'postalCode'      => $request->postalCode,
            'tel'             => $request->tel,
            'country'         => $request->country,
            'address'         => $request->address,
            'name'            => $request->name,
            'gender'          => $request->gender,
            ]);



        Session::flash('success', 'You application has been received, we will get back to you shortly');


        return redirect()->route('welcome');
    }

    public function enquiry(Request $request)
    {
        $request->validate([
            "name"      =>  'required',
            "email"     =>  'required',
            "phone"     =>  'required|int',
            "message"   => 'required'
        ]);

        Enquiry::create([
            "name"      =>  $request->name,
            "email"     =>  $request->email,
            "phone"     =>  $request->phone,
            "message"   => $request->message
        ]);

        Session::flash('success', 'We have received your message, we will get back to you shortly. Thank you');

        return redirect()->back();
    }


}
