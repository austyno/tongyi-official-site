<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Project;
use App\Application;
use App\Vacancy;
use App\Enquiry;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        //completed projects
        $completedProjects = Project::where(['status' => 'completed'])->get();

        //ongoing projects
        $ongoingProjects = Project::where(['status' => 'ongoing'])->get();

        //applications for employment
        $applications = Application::all();

        //posted vacancies
        $vacancies = Vacancy::all();

        //published news
        $publishedNews = News::where(['published' => 'yes'])->get();

        //not published
        $unpublished = News::where(['published' => 'no'])->get();

        //enquiries
        $enquiries = Enquiry::all();


        return view('home')->with([
            'publishedNews'         =>  $publishedNews,
            'completedProjects'     =>  $completedProjects,
            'ongoingProjects'       =>  $ongoingProjects,
            'applications'          =>  $applications,
            'vacancies'             =>  $vacancies,
            'enquiries'             =>  $enquiries,
            'unpublished'           =>  $unpublished


        ]);
    }
}
