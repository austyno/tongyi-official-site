<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\News;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allNews = News::orderBy('created_at', 'desc')->get();


        // dd($allNews);
        return view('admin.pages.news.index')->with([
                    'news' => $allNews
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.news.addNews');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->publish);

        $request->validate([
            'title'     => 'required|min:10',
            'publish'   => 'required',
            'content'   => 'required'
        ]);

        if($request->image){
            $image = $request->image;

            $splitImageName = preg_split('/\s+/',$image->getClientOriginalName());

            $imageName = $splitImageName > 1 ? $splitImageName[0] : $image->getClientOriginalName();

            $newName = time().'_'.$imageName;

            $image->move('lib/images/news/', $newName);

            News::create([
                'title'     => $request->title,
                'content'   => $request->content,
                'published' => $request->publish,
                'slug'      => Str::slug($request->title,'-'),
                'image'     => 'lib/images/news/'.$newName
            ]);

            Session::flash('success', 'News item created successfully');

            return redirect()->route('news');

        }else{

            News::create([
                'title'     => $request->title,
                'content'   => $request->content,
                'published' => $request->publish,
                'slug'      => Str::slug($request->title,'-')

            ]);

            Session::flash('success', 'News item created successfully');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $item = News::where(['slug' => $slug])->get();

        return view('admin.pages.news.view')->with([
            'item' => $item
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newsItem = News::find($id);
        
        return view('admin.pages.news.edit')->with([
            'newsItem' => $newsItem,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->image){

            $image = $request->image;

            $splitImageName = preg_split('/\s+/',$image->getClientOriginalName());

            $imageName = $splitImageName > 1 ? $splitImageName[0] : $image->getClientOriginalName();

            $newName = time().'_'.$imageName;
            
            $image->move('lib/images/news/', $newName);

            News::where(['id' => $id])->update([
                'title'     => $request->title,
                'content'   => $request->content,
                'image'     => 'lib/images/news/'.$newName,
                'slug'      => Str::slug($request->title,'-'),
                'published' =>  $request->publish

            ]);

            Session::flash('success','news item updated successfully');

            return redirect()->route('news');

        }else{

            News::where(['id' => $id])->update([
                'title'     => $request->title,
                'content'   => $request->content,
                'slug'      => Str::slug($request->title,'-'),
                'published' =>  $request->publish

            ]);

            Session::flash('success','news item updated successfully');

            return redirect()->route('news');

        }
        // dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id= $request->id;

        $delItem = News::find($id);

        $del = $delItem->delete();

        if(file_exists($delItem->image)){
            unlink($delItem->image);
        }


         if($del){
            return json_encode('successful');
         }else{
            return json_encode('failed');

         }
        

    }

    public function togglePublished($status,$id)
    {
        if($status === 'YES'){

            News::where(['id' => $id])->update(['published' => 'NO']);

            Session::flash('success','news unpublished');
            return redirect()->back();

        }elseif($status === 'NO'){

             News::where(['id' => $id])->update(['published' => 'YES']);

            Session::flash('success','news published');

            return redirect()->back();
        }
    }
}
