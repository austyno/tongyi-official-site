<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enquiry;
use Session;

class EnquiryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $enquiries = Enquiry::orderBy('created_at','desc')->get();

        return view('admin.pages.enquiry.index')->with([
            'enquiries' =>  $enquiries
        ]);
    }

    public function message(Request $request)
    {
        $id = $request->id;
        $enquiry = Enquiry::find($id);

        return response()->json($enquiry);
         
    }

    public function destroy($id)
    {
        $del = Enquiry::find($id);

        $del->delete();

        Session::flash('success','enquiry deleted successfully');

        return redirect()->back();
    }
}
