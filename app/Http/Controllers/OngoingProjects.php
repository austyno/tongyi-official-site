<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Project;
use App\ProjectImage;
use Session;

class OngoingProjects extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::with('images')->where(['status' => 'ongoing'])->get();

        return view('admin.pages.projects.ongoing.index')->with([
            'projects'  =>  $projects
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.projects.ongoing.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'status' => 'required',
            'name'  =>  'required',
            'image' =>  'required'
        ]);

        $image = $request->image;
        
        $splitImageName = preg_split('/\s+/',$image->getClientOriginalName());

        $imageName = $splitImageName > 1 ? $splitImageName[0] : $image->getClientOriginalName();

        $newName = time().'_'.$imageName;
        
        $image->move('lib/images/projects/'.$request->status.'/',$newName);

        $newPro = Project::create([
            'name'  =>  $request->name,
            'status'    =>  $request->status,
            'slug'      =>  Str::slug($request->name,'-'),
            'image'     => 'lib/images/projects/'.$request->status.'/'.$newName
        ]);

        ProjectImage::create([
            'project_id'    => $newPro->id,
            'image'     => 'lib/images/projects/'.$request->status.'/'.$newName
        ]);

        Session::flash('success','new ongoing project created');

        return redirect()->route('ongoing.all');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editProject = Project::find($id);
        return view('admin.pages.projects.ongoing.edit')->with(['edit' => $editProject]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editProject = Project::find($id);

        if($request->image == null){
            $editProject->update([
            'name'      => $request->name,
            'image'     => $editProject->image,
            'status'    => $request->status,
            'slug'      => Str::slug($request->name,'-')
            ]);

            Session::flash('success','project updated successfully');

            return redirect()->route($request->status.'.all');

        }else{

            $image = $request->image;


            $splitImageName = preg_split('/\s+/',$image->getClientOriginalName());

            $imageName = $splitImageName > 1 ? $splitImageName[0] : $image->getClientOriginalName();

            $newName = time().'_'.$imageName;

            //remove old image from server
            if(file_exists($editProject->image)){
                unlink($editProject->image);
            }

            //remove old image from project images
             $oldImage =  ProjectImage::where(['project_id' => $id,'image' => $editProject->image])->get();

                if(!empty($oldImage[0]->image)){
                    $oldImage[0]->delete();
                }


            //upload new image
            $image->move('lib/images/projects/'.$request->status.'/' , $newName);

            $editProject->update([
            'name'      => $request->name,
            'image'     => 'lib/images/projects/'.$request->status.'/'.$newName,
            'status'    => $request->status,
            'slug'      => Str::slug($request->name,'-')
            ]);

            //insert into project images
            ProjectImage::create([
                'project_id' => $id,
                'image'     =>  'lib/images/projects/'.$request->status.'/'.$newName,
            ]);

            Session::flash('success','project updated successfully');

            return redirect()->route($request->status.'.all');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        //find image from project images 
        $delImg = ProjectImage::find($id);

        //use image project_id and locate project so we can delete image from project
        $project = Project::find($delImg->project_id);


        //remove image from server
        if(file_exists($delImg->image)){
            unlink($delImg->image);

        }

        //delete image from project images
        $del = $delImg->delete();

        //remove image from projects 
        Project::where(['id' => $project->id])->update(['image' => null]);



        if($del){
            return json_encode('successful');
         }else{
            return json_encode('failed');

         }


    }

    public function viewImages($id)
    {

        $images = ProjectImage::with('project')->where(['project_id' => $id])->get();

        //  dd($images);

        if($images->count() > 0){

            return view('admin.pages.projects.completed.images')->with([
                    'images'    =>  $images
            ]);

        }else{
            
            //find project so we can upload images to it
            $project = Project::find($id);
            
            $proId  = $project->id;
            $status = $project->status;
            $name   = $project->name;

            return view('admin.pages.projects.ongoing.uploadImage')->with([
                        'id'     => $proId,
                        'status' => $status,
                        'name'   => $name
                        ]);

        }

        
    }

    public function newImages($proId,$status,$name)
    {

        return view('admin.pages.projects.ongoing.uploadImage')->with([
            'id' => $proId,
            'status' => $status,
            'name'  => $name
            ]);
    }


    public function imageUpld(Request $request,$proId,$status)
    {
        $image = $request->file('file');

        //rename image
        $splitImageName = preg_split('/\s+/',$image->getClientOriginalName());

        $imageName = $splitImageName > 1 ? $splitImageName[0] : $image->getClientOriginalName();

        $newName = time().'_'.$imageName;

        //upld to server

        $image->move('lib/images/projects/'.$status.'/',$newName);

        //store in DB
        ProjectImage::create([
            'project_id'  => $proId,
            'image'       => 'lib/images/projects/'.$status.'/'.$newName
        ]);

        return response()->json(['success' => $newName]);

        
    }

    public function deleteOngoingProject($id)
    {
        //locate project to be deleted using provided id
        $projectToDelete = Project::find($id);      

        //locate all images belonging to project to delete and delete from project images
        $projectToDeleteImages = ProjectImage::where(['project_id' => $id])->get();

        foreach($projectToDeleteImages as $projectImage){
            
            if(file_exists($projectImage->image)){
                //delete from server
                unlink($projectImage->image);
            }

            //delete from db
            $projectImage->delete();

        }

        //delete project
        $projectToDelete->delete();


        Session::flash('success','The project was deleted successfully');

        return redirect()->back();
    }
}
