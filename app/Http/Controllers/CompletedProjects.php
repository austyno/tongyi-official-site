<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Project;
use App\ProjectImage;
use Session;


class CompletedProjects extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $projects = Project::with('images')->where(['status' => 'completed'])->get();

        return view('admin.pages.projects.completed.index')->with([
            'projects'  =>  $projects
        ]);

    }

    public function create()
    {
        return view('admin.pages.projects.completed.new');
    }

    public function store(Request $request)
    {
        // dd($request->image);
        $request->validate([
            'status' => 'required',
            'name'  =>  'required',
            'image' =>  'required'
        ]);

        $image = $request->image;

        $splitImageName = preg_split('/\s+/',$image->getClientOriginalName());

        $imageName = $splitImageName > 1 ? $splitImageName[0] : $image->getClientOriginalName();


        $newName = time().'_'.$imageName;
        
        $image->move('lib/images/projects/'.$request->status.'/',$newName);

        $newPro = Project::create([
            'name'  =>  $request->name,
            'status'    =>  $request->status,
            'slug'      =>  Str::slug($request->name,'-'),
            'image'     => 'lib/images/projects/'.$request->status.'/'.$newName
        ]);

        ProjectImage::create([
            'project_id'  =>   $newPro->id,
            'image'     => 'lib/images/projects/'.$request->status.'/'.$newName


        ]);

        Session::flash('success','new completed project created');

        return redirect()->route('completed.all');

    }



    public function viewImages($id)
    {

        $images = ProjectImage::with('project')->where(['project_id' => $id])->get();

        //  dd($images);

        if($images->count() > 0){

            return view('admin.pages.projects.completed.images')->with([
                    'images'    =>  $images
            ]);

        }else{
            
            //find project so we can upload images to it
            $project = Project::find($id);
            
            $proId  = $project->id;
            $status = $project->status;
            $name   = $project->name;

            return view('admin.pages.projects.ongoing.uploadImage')->with([
                        'id'     => $proId,
                        'status' => $status,
                        'name'   => $name
                        ]);

        }

        
    }



    public function newImages($proId,$status,$name)
    {

        return view('admin.pages.projects.ongoing.uploadImage')->with([
            'id'     => $proId,
            'status' => $status,
            'name'   => $name
            ]);
    }

    

    public function imageUpld(Request $request,$proId,$status)
    {
        $image = $request->file('file');


        $splitImageName = preg_split('/\s+/',$image->getClientOriginalName());

        $imageName = $splitImageName > 1 ? $splitImageName[0] : $image->getClientOriginalName();

        //rename image
        $newName = time().'_'.$imageName;

        //upld to server
        $image->move('lib/images/projects/'.$status.'/',$newName);

        //store in DB
        ProjectImage::create([
            'project_id'  => $proId,
            'image'       => 'lib/images/projects/'.$status.'/'.$newName
        ]);

        return response()->json(['success' => $newName, 'status' => $status]);

        
    }

    public function edit($id)
    {
        $editProject = Project::find($id);
        return view('admin.pages.projects.completed.edit')->with(['edit' => $editProject]);
    }

    public function update(Request $request, $id)
    {
        $editProject = Project::find($id);

        if($request->image == null){
            $editProject->update([
            'name'      => $request->name,
            'image'     => $editProject->image,
            'status'    => $request->status,
            'slug'      => Str::slug($request->name,'-')
            ]);

            Session::flash('success','project updated successfully');

            return redirect()->route('completed.all');

        }else{

            $image = $request->image;


            $splitImageName = preg_split('/\s+/',$image->getClientOriginalName());

            $imageName = $splitImageName > 1 ? $splitImageName[0] : $image->getClientOriginalName();

            $newName = time().'_'.$imageName;

            //remove old image from server
            if(file_exists($editProject->image)){
                unlink($editProject->image);
            }

            //remove old image from project images
             $oldImage =  ProjectImage::where(['image' => $editProject->image])->get();

                if(!empty($oldImage[0]->image)){
                    $oldImage[0]->delete();
                }
            

            //upload new image
            $image->move('lib/images/projects/'.$request->status.'/' , $newName);

            $editProject->update([
            'name'      => $request->name,
            'image'     => 'lib/images/projects/'.$request->status.'/'.$newName,
            'status'    => $request->status,
            'slug'      => Str::slug($request->name,'-')
            ]);

            //insert into project images
            ProjectImage::create([
                'project_id' => $id,
                'image'     =>  'lib/images/projects/'.$request->status.'/'.$newName,
            ]);

            Session::flash('success','project updated successfully');

            return redirect()->route($request->status.'.all');

        }
    }


    public function deleteOngoingProject($id)
    {
        //locate project to be deleted using provided id
        $projectToDelete = Project::find($id);      

        //locate all images belonging to project to delete and delete from project images
        $projectToDeleteImages = ProjectImage::where(['project_id' => $id])->get();

        foreach($projectToDeleteImages as $projectImage){
            
            if(file_exists($projectImage->image)){
                //delete from server
                unlink($projectImage->image);
            }

            //delete from db
            $projectImage->delete();

        }

        //delete project
        $projectToDelete->delete();


        Session::flash('success','The project was deleted successfully');

        return redirect()->back();
    }
}
