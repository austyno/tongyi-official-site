<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable=[
        'surename','name','address','country','postalCode','tel','email',
            'specialization','coverLetter','resume','gender'
    ];
}
