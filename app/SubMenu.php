<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    protected $fillable =[
        'menu_id','name'
    ];

    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
}
