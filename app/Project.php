<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name','image','status','slug'
    ];

    public function images()
    {
        return $this->hasMany('App\ProjectImage');
    }
}
