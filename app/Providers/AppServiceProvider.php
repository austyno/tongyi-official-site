<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Cache\Factory;
use Illuminate\Contracts\Cache\Repository;
use App\Menu;
use App\News;
use View;
use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //menu
      $menu = Cache::remember('menus',60,function(){
          return  Menu::with('submenus')->get();
        });

        $footerNews = News::where(['published' => 'YES'])->orderBy('created_at', 'desc')->get()->take(2);


        View::share(['menus' => $menu, 'footerNews' => $footerNews]);
    }
}
