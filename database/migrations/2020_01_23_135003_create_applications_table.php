<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('surename');
            $table->string('name');
            $table->string('address');
            $table->string('country');
            $table->string('postalCode');
            $table->integer('tel');
            $table->string('email');
            $table->string('specialization');
            $table->text('coverLetter');
            $table->string('gender');
            $table->string('resume');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
