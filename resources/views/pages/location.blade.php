@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

            <!-- SECTION CONTENTG START -->
            <div class="section-full p-tb80 inner-page-padding">
                <!-- LOCATION BLOCK-->
                <div class="container">
                
                    <!-- GOOGLE MAP & CONTACT FORM -->
                    {{-- <div class="section-content">
                        <!-- CONTACT FORM-->
                    </div> --}}
                    
                    <div class="gmap-outline">
                        <div id="gmap_canvas2" class="google-map">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3939.8816898136993!2d7.430256714664851!3d9.0745416934893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e7529594e9aff%3A0xe3a4f7ba1816d908!2s105%20Ebitu%20Ukiwe%20St%2C%20Jabi%2C%20Abuja!5e0!3m2!1sen!2sng!4v1578902600013!5m2!1sen!2sng" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

                        </div>

                    </div>                    
                </div>
           </div>
         
            <!-- SECTION CONTENT END -->
            
        </div>
        <!-- CONTENT END -->
@endsection