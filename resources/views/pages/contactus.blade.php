@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

<div class="section-full p-tb80 inner-page-padding">
                <!-- LOCATION BLOCK-->
                <div class="container">

                    <!-- GOOGLE MAP & CONTACT FORM -->
                    <div class="section-content">
                        <!-- CONTACT FORM-->
                        <div class="row">
                            <div class="col-md-8 col-sm-6">
                                
                                <form class="contact-form cons-contact-form" method="post" action="{{route('enquiry')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="contact-one m-b30">

                                        <!-- TITLE START -->
                                        <div class="section-head">
                                            <div class="mt-separator-outer separator-left">
                                                <div class="mt-separator">
                                                    <h2 class="text-uppercase sep-line-one "><span
                                                            class="font-weight-300 text-primary">Get</span> In touch
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- TITLE END -->
                                        <div class="form-group">
                                            <input name="name" type="text" required class="form-control"
                                                placeholder="Name">
                                        </div>

                                        <div class="form-group">
                                            <input name="email" type="text" class="form-control" required
                                                placeholder="Email">
                                        </div>

                                        <div class="form-group">
                                            <input name="phone" type="text" class="form-control" required
                                                placeholder="Phone Number">
                                        </div>

                                        <div class="form-group">
                                            <textarea name="message" rows="4" class="form-control" required
                                                placeholder="Message"></textarea>
                                        </div>

                                        <div class="text-right">
                                            <button  type="submit" value="Submit" class="site-button btn-effect">
                                                submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="contact-info m-b30">
                                    <!-- TITLE START -->
                                    <div class="section-head">
                                        <div class="mt-separator-outer separator-left">
                                            <div class="mt-separator">
                                                <h2 class="text-uppercase sep-line-one "><span
                                                        class="font-weight-300 text-primary">Contact</span> Info</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- TITLE END -->
                                    <div class="bg-dark p-a20 text-white">
                                        <div class="mt-icon-box-wraper left p-b40">
                                            <div class="icon-xs"><i class="fa fa-phone"></i></div>
                                            <div class="icon-content">
                                                <h5 class="m-t0 font-weight-500">Phone number</h5>
                                                <p>+2348171966666</p>
                                            </div>
                                        </div>

                                        <div class="mt-icon-box-wraper left p-b40">
                                            <div class="icon-xs"><i class="fa fa-envelope"></i></div>
                                            <div class="icon-content">
                                                <h5 class="m-t0 font-weight-500">Email address</h5>
                                                <p>info@tongyigroupintl.com</p>
                                            </div>
                                        </div>

                                        <div class="mt-icon-box-wraper left">
                                            <div class="icon-xs"><i class="fa fa-map-marker"></i></div>
                                            <div class="icon-content">
                                                <h5 class="m-t0 font-weight-500">Address info</h5>
                                                <p>Plot 105，Ebitu Ukiwe Street，Jabi，Abuja，Nigeria</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="gmap-outline">
                        <div id="gmap_canvas2" class="google-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31519.49484758172!2d7.414512140311372!3d9.069517383528487!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e74d61ad1a8ef%3A0x171deef23996a411!2sTONGYI%20GROUP%20NIGERIA%20LTD!5e0!3m2!1sen!2sng!4v1579116515844!5m2!1sen!2sng" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>


@endsection