@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))
<div class="container"><br><br>
<div class="section-head">
      <div class="mt-separator-outer separator-left">
          <div class="mt-separator">
              <h2 class="text-black text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Apply </span> Now</h2>
          </div>
      </div>
  </div>
    <div class="row">
      <h5  style="text-align:center">Complete application must include both a CV and a cover letter.<br> Incomplete applications will not be considered.<br>Please check the vacancies page before filling the applications form.</h5><br><br>
    </div>
  
  <div class="row">
    <div class="col-md-8 col-sm-6 col-xs-12">
      @include('../inc/error')
    </div>
  </div>
  <fieldset style="margin-bottom:150px">
    
  <form class="form-horizontal form-label-left" method="post" action="{{route('apply')}}" enctype="multipart/form-data">
    @csrf
  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="surename">Surename<span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" name="surename" value="{{old('surename')}}" required="required" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" name="name" value="{{old('name')}}" required="required" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" name="address" value="{{old('address')}}" required="required" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Country <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" name="country" value="{{old('country')}}" required="required" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Postal Code/City <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" name="postalCode" value="{{old('postalCode')}}" required="required" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telephone <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="number" name="tel" value="{{old('tel')}}" required="required" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required"> *</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="email" name="email" value="{{old('email')}}" class="form-control col-md-7 col-xs-12" >
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Field <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <select name="specialization">
        @foreach ($jobs as $job)
          <option value="{{$job->title}}">{{$job->title}}</option>
        @endforeach
      </select>
      {{-- <input type="text" name="specialization" value="{{old('specialization')}}" class="date-picker form-control col-md-7 col-xs-12" required="required" > --}}
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <select name="gender">
        <option value="male">Male</option>
        <option value="female">Female</option>
      </select>
    </div>
  </div> 
  

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Cover Letter <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <textarea name="coverLetter" class="date-picker form-control col-md-7 col-xs-12" rows="6" required="required">{{old('coverLetter')}}</textarea>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12"><i class="fa fa-info-circle" style="font-size:15px" data-toggle="tooltip" title="resume must be in pdf or doc types"></i> Upload Resume <span class="required" style="font-size:16px">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
       <input type="file" class="custom-file-input" name="resume">
    </div>
  </div>

  


  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
      <button type="submit" class="btn btn-success">Submit</button>
    </div>
  </div>

</form>
  </fieldset>
</div>



@endsection