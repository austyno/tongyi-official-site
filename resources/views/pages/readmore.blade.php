@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($newsItem[0]->title))

<div class="section-full p-tb80 inner-page-padding">
        <div class="container">

          <div class="blog-post date-style-3 blog-detail text-black">
            <div class="mt-post-media clearfix m-b30">
              <ul class="grid-post">
                <li>
                  <div class="portfolio-item">
                    <img class="img-responsive" src="{{asset($newsItem[0]->image)}}" alt="">
                  </div>
                </li>
              </ul>
            </div>

            <div class="mt-post-meta ">
              <ul>
              <li class="post-date"><strong>{{$newsItem[0]->created_at->format('d')}} </strong> <span>{{$newsItem[0]->created_at->format('M')}} {{$newsItem[0]->created_at->format('Y')}}</span> </li>
                <li class="post-author"><a href="javascript:void(0);">By <span>Admin</span></a> </li>
              </ul>
            </div>

            <div class="mt-post-title ">
              <h2 class="post-title"><a href="javascript:void(0);" class="font-weight-600">{{$newsItem[0]->title}}</a></h2>
            </div>

            <div class="mt-post-text" style="text-align:justify;text-justify:inter-word;font-size:15px;;">
              <p style="line-height:30px;">
                {{$newsItem[0]->content}}
              </p>
              <blockquote class="bg-dark text-white author-quote">
                <h4 class="m-b0"><i class="fa fa-quote-left"></i>&nbsp;<br>We let our quality work and commitment to customer satisfaction be our slogan. Quality
                  you deserve and dependability you can count on.<i class="fa fa-quote-right"></i> </h4>
              </blockquote>
            </div>
          </div>

          <!-- OUR BLOG START -->

          <!-- TITLE START -->
          <div class="section-head ">
            <div class="mt-separator-outer separator-left">
              <div class="mt-separator">
                <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Latest</span> News
                </h2>
              </div>
            </div>
          </div>
          <!-- TITLE END -->

          <!-- IMAGE CAROUSEL START -->
          <div class="section-content">
            <div class="row">

              @foreach ($news as $item)

              <div class="col-md-4 col-sm-6">
                <div class="mt-box blog-post latest-blog-3 date-style-1 bg-white m-b30">
                  <div class="mt-post-media mt-img-overlay7">
                  <a href="javascript:;"><img src="{{asset($item->image)}}" alt=""></a>
                  </div>
                  <div class="mt-post-info p-a30 bg-gray">
                    <div class="post-overlay-position">
                      <div class="mt-post-meta ">
                        <ul>
                        <li class="post-date"><strong class="text-primary">{{$item->created_at->format('d')}}</strong> <span>{{$item->created_at->format('M')}} {{$item->created_at->format('Y')}}</span></li>
                          <li class="post-author">By <a href="javascript:;">Admin</a> </li>
                        </ul>
                      </div>
                      <div class="mt-post-title ">
                      <h4 class="post-title m-b0">{{$item->title}}</h4>
                      </div>
                      <div class="mt-post-text">
                        <p>
                          {{substr($item->content,0,100)}}
                        </p>
                      </div>
                      <div class="readmore-line">
                      <span><a href="{{route('readmore',['slug' => $item->slug])}}" class="site-button-link" data-hover="Read More">Read More <i
                              class="fa fa-angle-right arrow-animation"></i></a></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                  
              @endforeach
            </div>
          </div>

          <!-- OUR BLOG END -->

        </div>
      </div>
@endsection

