@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

			<!-- SECTION CONTENT START -->
            <div class="section-full p-tb80 inner-page-padding">
            	<div class="container">

              <div class="section-head">
                <div class="mt-separator-outer separator-left">
                  <div class="mt-separator">
                    <h2 class="text-black text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Image</span> Gallery</h2>
                  </div>
                </div>
              </div>
                    
                    <!-- GALLERY CONTENT START -->
  <div class="portfolio-wrap mfp-gallery work-grid row clearfix">

  @foreach ($galleryImages as $img)  
    <!-- COLUMNS 1 -->
      <div class="masonry-item col-md-4 col-sm-6 m-b30">
          <div class="image-effect-two hover-shadow">
            <img src="{{asset($img->image)}}" alt="" style="height:270px;width:360px"/>
              <div class="figcaption">
                <h4 class="mt-tilte">{{$img->title}}</h4>
                <a class="mfp-link" href="{{asset($img->image)}}" title="{{$img->title}}" style="margin-left:40%">
                    <i class="fa fa-arrows-alt"></i>
                  </a>
              </div>
          </div>
      </div> 
  @endforeach
    </div>
  </div>
    <div class="m-t50 text-center">
      {{$galleryImages->links()}}
    </div>
</div>


@endsection