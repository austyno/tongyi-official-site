@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

<div class="section-full p-tb80 bg-white inner-page-padding">
    <div class="container">
        <!-- TITLE START -->
        <div class="section-head">
            <div class="mt-separator-outer separator-left">
                <div class="mt-separator">
                    <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Group </span> Introduction</h2>
                </div>
            </div>
        </div>                   
        <!-- TITLE END -->                 
        <div class="section-content ">
            <div class="row">
                <div class="col-md-5 col-sm-6">
                    <div class="m-about ">
                        <div class="owl-carousel about-us-carousel">
                            <!-- COLUMNS 1 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic8.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic2.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>

                            <!-- COLUMNS 5 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic1.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                        
                <div class="col-md-7 col-sm-6">
                    <div class="m-about-containt text-black p-t80">
                        <div class="m-about-years bg-moving" style="background-image:url({{asset('lib/images/background/line.png')}});">
                            <span class="text-primary large-title"></span>
                            <span class="large-title-info">We continue to promote scientific and technological progress, with 16 major scientific research successes</span>
                        </div>
                        <h3 class="font-weight-600" style="font-size:18px;line-height:24px;font-family:font-family: -apple-system, system-ui, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;"></h3>
                            <p>One of the group was established in 1997, since the establishment of the company, has been "good faith compliance, quality assurance, safety first" principle, the company's main business areas including roads, bridges, tunnels, traffic engineering, municipal engineering construction, housing construction, Large-scale shopping malls, thermal power stations & technology development and consulting, testing and other related auxiliary industries.</p><p> The company has the qualification of general contract of highway construction, first class qualification of subgrade, pavement, bridge, tunnel and earthwork engineering, It has passed the QEOHS certification and is capable of undertaking all kinds of construction projects including domestic and international highways, municipalities, bridges, reservoirs, power stations, buildings, environmental protection and mining.</p>
                            <p>Group companies in accordance with the modern enterprise system set up a specializes in mining development company - Nigeria Huasheng Group. Is a non-ferrous metal and other mineral resources exploration, precocious selection, smelting, deep processing and sales-based modern mining company.
                                Over the past decade or so, the company has been awarded as "Honorable Contract and Trustworthy Unit", "Excellent Unit for Security Compliance", "Top 100 Construction Enterprises in the Province", "Heavy Safety Demonstration Unit" and "Shenzhen Labor and Social Security Unit".</p>
                            <div class="text-right">
                              <a href="#" class="site-button-link" data-hover="Read More" data-toggle="modal" data-target="#large-Modal">Read More <i class="fa fa-angle-right arrow-animation"></i></a>
                            </div>
                            </p>                                       
                        <div class="author-info p-t20">
                        <a href="{{route('download')}}" class="site-button btn-effect m-b15"><span>Download Profile</span></a>                                        
                        </div> 
                    </div>
                </div>                                

            </div>
        </div>
    </div>
</div> 


<div id="large-Modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- MODAL CONTENT -->
    <div class="modal-content">
        <div class="modal-header bg-secondry">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-white">Group Introduction</h4>
        </div>
        <div class="modal-body">
        <p> One of the group was established in 1997, since the establishment of the company, has been "good faith compliance, quality assurance, safety first" principle, the company's main business areas including roads, bridges, tunnels, traffic engineering, municipal engineering construction, housing construction, Large-scale shopping malls, thermal power stations and technology development and consulting, testing and testing and other related auxiliary industries.</P>
        <p> The company has the qualification of general contract of highway construction, first class qualification of subgrade, pavement, bridge, tunnel and earthwork engineering, It has passed the QEOHS certification and is capable of undertaking all kinds of construction projects including domestic and international highways, municipalities, bridges, reservoirs, power stations, buildings, environmental protection and mining.</p>
       <p>Group companies in accordance with the modern enterprise system set up a specializes in mining development company - Nigeria Huasheng Group. Is a non-ferrous metal and other mineral resources exploration, precocious selection, smelting, deep processing and sales-based modern mining company.</p>
      <p>Over the past decade or so, the company has been awarded as "Honorable Contract and Trustworthy Unit", "Excellent Unit for Security Compliance", "Top 100 Construction Enterprises in the Province", "Heavy Safety Demonstration Unit" and "Shenzhen Labor and Social Security Unit".</p>
       <p>The company has about 2,300 staff and workers, 225 various types of professional and technical personnel, 19 first-level architects and over 300 sets of medium- and large-sized advanced construction machinery and equipment, with high-grade highways capable of operating over 150km at the same time and 16 Block special large bridge, the output value of more than 3 billion yuan in production capacity.</p>
      <p>The company successively took part in the construction of over 1,200 kilometers of high-grade highways in more than 20 provinces and autonomous regions in China, built more than 130 large and medium-sized bridges and went abroad to participate in the technical assistance and highway and municipal projects in Cameroon. The projects undertaken by the Company have achieved outstanding results in terms of quality and safety, construction schedule and civilized construction. </p><p>The excellent and good project rate reached over 98%, which was praised by construction units and industry authorities. Among them, a number of highly complex projects with complex structures and high technical contents have been completed, such as Fujian's Wuyishan-Shaowu Expressway, Sichuan's Qionglai-Mingshan Expressway, Chongqing Pengshui Hydropower Station's approach road, Yinpan Reservoir, Yalongjiang Jinping I Hydropower Station Xiamen Road extension project, Pingyao Street to Yanshan Expressway and won the Silver Medal of National Quality Project, the First Prize of Quality Project of Ministry of Communications, Sichuan-Chengdu-Jianyang Expressway and Guiyang-Guangzhou Railway Passenger Dedicated Line.</p>
       <p>The company continued to promote scientific and technological progress, with 16 major scientific research successes respectively provincial, ministerial and national awards, of which two reached the international advanced level, won the United States Pittsburgh National Invention Exposition silver and bronze awards.</p>
        <p>Companies adhering to the "integrity-based, boutique based on the market; scientific harmony and innovation cast the enterprise" business philosophy, external to meet the needs of owners as its mission, the pursuit of happiness and sense of accomplishment within the staff to maximize the sense in the whole industry chain , Create value for the community, build quality projects, provide first-class service!</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="site-button btn-effect button-sm text-uppercase letter-spacing-2" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
</div>


@endsection