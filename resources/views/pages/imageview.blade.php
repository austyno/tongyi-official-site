@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))


<div class="section-full p-tb80 inner-page-padding">
  <div class="container">
    <div class="section-head">
      <div class="mt-separator-outer separator-left">
      <div class="mt-separator">
      <h2 class="text-black text-uppercase sep-line-one "><span class="text-primary" style="font-size:20px">{{ucwords($crumb)}}</span><span style="font-size:20px"> Project Images</span></h2>
      </div>
    </div>
  </div>
    <div class="container">
      <div class="portfolio-wrap mfp-gallery work-grid row clearfix">

          @foreach ($images as $image)

            <div class="masonry-item col-md-4 col-sm-6 m-b30">
              <div class="image-effect-two hover-shadow">
                <img src="{{asset($image->image)}}" alt="" style="height:270px;width:360px"/>
                <div class="figcaption">
                <h4 class="mt-tilte">{{ucwords($image->project->name)}}</h4>
                <a class="mfp-link" href="{{asset($image->image)}}" style="margin-left:40%" title="{{$image->project->name}}">
                    <i class="fa fa-arrows-alt"></i>
                  </a>
                </div>
              </div>
              <div class="mt-info p-t20">
                {{-- <h4 class="mt-title m-b20 m-t0" style="color:#5B85B6"><a href="#">{{ucwords($image->project->name)}}</a></h4> --}}
              </div>

              {{-- <p style="font-size:16px;font-weight:bold;text-align:center;color:white;height:50px;background:#5B85B6">{{ucwords($image->project->name)}}</p> --}}
            </div>
              
          @endforeach
      </div>
      <div class="m-t50 text-center">
      <a href="{{ url()->previous() }}" class="site-button btn-effect m-b15" tool-tip="back to completed projects"><span><i class="fa fa-angle-double-left arrow-animation"></i> <i class="fa fa-angle-double-left arrow-animation"></i> </span></a>                                         
      </div>
    </div>
</div>
@endsection