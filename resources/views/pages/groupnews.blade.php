@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

<div class="section-full p-tb80 bg-white inner-page-padding">

        <!-- GALLERY CONTENT START -->
        <div class="container">
          <div class="portfolio-wrap mfp-gallery news-grid clearfix row ">

            <!-- COLUMNS 1 -->
            @foreach ($news as $n)

            <div class="masonry-item  col-lg-4 col-md-4 col-sm-6">
              <div class="blog-post blog-grid date-style-2">

                <div class="mt-post-media mt-img-effect zoom-slow">
                <a href="javascript:void(0);"><img src="{{asset($n->image)}}" alt="" style="height:240px;width:360px"></a>
                </div>

                <div class="mt-post-info p-t30">

                  <div class="mt-post-title ">
                  <h4 class="post-title"><a href="javascript:void(0);">{{$n->title}}</a></h4>
                  </div>

                  <div class="mt-post-meta ">
                    <ul>
                    <li class="post-date"> <i class="fa fa-calendar"></i><strong>{{$n->created_at->format('d')}}</strong> <span>{{$n->created_at->format('M')}}</span><span>{{$n->created_at->format('Y')}}</span> </li>
                      <li class="post-author"><i class="fa fa-user"></i><a href="javascript:void(0);">By
                          <span>Admin</span></a> </li>
                    </ul>
                  </div>

                  <div class="mt-post-text">
                  <p>{{substr($n->content,0,200)}}</p>
                  </div>

                  <div class="clearfix">
                    <div class="mt-post-readmore pull-left">
                      <a href="{{route('readmore',['slug' => $n->slug])}}" title="READ MORE" rel="bookmark" class="site-button-link">Read
                        More<i class="fa fa-angle-right arrow-animation"></i></a>
                    </div>
                    <div class="widget_social_inks pull-right">
                      <ul class="social-icons social-radius social-dark m-b0">
                        <li><a href="javascript:void(0);" class="fa fa-facebook"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-rss"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-youtube"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-instagram"></a></li>
                      </ul>
                    </div>
                  </div>

                </div>

              </div>
            </div>      
            @endforeach
          </div>
          <ul class="pagination m-tb0">
            {{$news->links()}}
          </ul>
        </div>
        <!-- GALLERY CONTENT END -->

      </div>

@endsection
