@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

<div class="m-b50" style="margin:150px">
                                    
  <!-- TITLE START -->
  <div class="section-head">
      <div class="mt-separator-outer separator-left">
          <div class="mt-separator">
              <h2 class="text-black text-uppercase sep-line-one "><span class="font-weight-300 text-primary">We Are</span> Hiring</h2>
          </div>
      </div>
  </div>                   
  <!-- TITLE END -->                                    
  
  <!-- TAB DEFAULT WITH NAV BG -->
  <div class="section-content">
      <div class="mt-tabs bg-tabs">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#basic">Basic Requirements</a></li>

              @foreach ($jobs as $job)
              @php
                  $t = preg_split('/\s+/',$job->title);
                  $title = $t > 1 ? $t[0] : $job->title;
              @endphp
                <li><a data-toggle="tab" href="#{{$title}}">{{$job->title}}</a></li>
              @endforeach
              {{-- <li><a data-toggle="tab" href="#reserve">Reserve manager</a></li>
              <li><a data-toggle="tab" href="#surveyor">Surveyor</a></li>
              <li><a data-toggle="tab" href="#accountant">Accounting</a></li>
              <li><a data-toggle="tab" href="#project-management">Project management staff</a></li> --}}
          </ul>
          <div class="tab-content">
          
              <div id="basic" class="tab-pane active">
                <ol class="list-num-count" style="margin:30px;font-size:15px;line-height:30px">
                  <li>Bachelors degree or above.</li>
                  <li>Reserve management English required professional English four or more proficiency in English listening, speaking, reading and writing.</li>
                  <li>Surveyors 3 years working experience in measurement, work hard work.</li>
                  <li>Project management personnel required bachelor degree or above in engineering management, work experience is preferred.</li>
                  <li>Accounting Requirements Bachelor degree or above in finance, accounting certificate, work experience is preferred.</li>

                </ol>
              </div>

              @foreach ($jobs as $job)

              @php
                  $t = preg_split('/\s+/',$job->title);
                  $title = $t > 1 ? $t[0] : $job->title;
              @endphp
                <div id="{{$title}}" class="tab-pane" style="font-size:15px;line-height:30px">
                  <table class="tabel table-hover">
                    <tr>
                      <th>Number Required</th>
                      <td>{{$job->slots}}</td>
                    </tr>
                    <tr>
                      <th>Department</th>
                      <td>{{ucwords($job->department)}}</td>
                    </tr>
                    <tr>
                      <th>Qualification</th>
                      <td>{{ucwords($job->qualification)}}</td>
                    </tr>

                  </table>
                </div>
                  
              @endforeach
              
          </div>
      </div>
  </div>
</div>


@endsection