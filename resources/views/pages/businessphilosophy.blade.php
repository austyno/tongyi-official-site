@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

<style>
  .bg-gray ul li{
    padding-bottom: 5px;
    list-style: none;
    cursor: pointer;
    margin-bottom: 8px;

    
  }

.box-wrap:hover .box:hover{
  transform:scale(.9);
  filter:blur(0px);
  opacity:1;
  padding: 10px;
  border-radius:5px;
  box-shadow:0 8px 20px 0px rgba(0,0,0,0.125);
}

</style>

<div class="section-full p-tb80 bg-white inner-page-padding">
    <div class="container">
        <!-- TITLE START -->
        <div class="section-head">
            <div class="mt-separator-outer separator-left">
                <div class="mt-separator">
                    <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Business </span> Philosophy</h2>
                </div>
            </div>
        </div>                   
        <!-- TITLE END -->                 
        <div class="section-content ">
            <div class="row">
                <div class="col-md-5 col-sm-6">
                    <div class="m-about ">
                        <div class="owl-carousel about-us-carousel">
                            <!-- COLUMNS 1 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic8.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic2.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>

                            <!-- COLUMNS 5 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic1.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                        
                <div class="col-md-7 col-sm-6">
                    <div class="m-about-containt text-black p-t80">
                        <div class="m-about-years bg-moving" style="background-image:url({{asset('lib/images/background/line.png')}});">
                            <span class="text-primary large-title"></span>
                            <span class="large-title-info">Over the past decade we have recieved several awards in the various domains we operate in</span>
                        </div>


                          <div class="col-md-6 col-sm-6 bg-gray p-a40 box-wrap">
                          <ul class="list-angle-right arrow-animation">
                            <li class="box">Entrepreneurial spirit</li>
                            <li class="box">Talent concept</li>
                            <li class="box">Unity, hard work, truth-seeking, innovation</li>
                            <li class="box">Career education, job success</li>
                            <li class="box">Corporate purposes</li>
                            <li class="box">Scientific harmony, innovation cast the enterprise</li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 bg-gray p-a40 p-b60 box-wrap">
                            <ul class="list-angle-right arrow-animation">
                              <li class="box">People-oriented, scientific norms</li>
                              <li class="box">Enterprise service strategy</li>
                              <li class="box">Honesty, quality based on the market</li>
                              <li class="box">Owners first, customers first</li>
                              <li class="box">Competition, service, management, benefit</li>
                              <li class="box">Business management concept</li>
                            </ul>
                        </div>


                          {{-- <div class="bg-gray p-a30">
                              <ul class="filter-navigation masonry-filter text-uppercase">
                                <li class="active"><a data-hover="All" href="#">All</a></li>
                                <li><a  data-hover="High Rise Buildings" href="javascript:;">High Rise Buildings</a></li>
                                <li><a data-hover="Tunnel Construction" href="javascript:;">Tunnel Construction</a></li>
                                <li><a data-hover="Residential" href="javascript:;">Residential</a></li>
                                <li><a data-hover="Maintainance" href="javascript:;">Maintainance</a></li>
                                </li>
                              </ul>
                          </div> --}}
                    </div>
                </div>                           

            </div>
        </div>
    </div>
</div> 

<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Group Introduction</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size:16px;line-height:24px;font-family:font-family: -apple-system, system-ui, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;">
       <p> One of the group was established in 1997, since the establishment of the company, has been "good faith compliance, quality assurance, safety first" principle, the company's main business areas including roads, bridges, tunnels, traffic engineering, municipal engineering construction, housing construction, Large-scale shopping malls, thermal power stations and technology development and consulting, testing and testing and other related auxiliary industries.</P>
        <p> The company has the qualification of general contract of highway construction, first class qualification of subgrade, pavement, bridge, tunnel and earthwork engineering, It has passed the QEOHS certification and is capable of undertaking all kinds of construction projects including domestic and international highways, municipalities, bridges, reservoirs, power stations, buildings, environmental protection and mining.</p>
       <p>Group companies in accordance with the modern enterprise system set up a specializes in mining development company - Nigeria Huasheng Group. Is a non-ferrous metal and other mineral resources exploration, precocious selection, smelting, deep processing and sales-based modern mining company.</p>
      <p>Over the past decade or so, the company has been awarded as "Honorable Contract and Trustworthy Unit", "Excellent Unit for Security Compliance", "Top 100 Construction Enterprises in the Province", "Heavy Safety Demonstration Unit" and "Shenzhen Labor and Social Security Unit".</p>
       <p>The company has about 2,300 staff and workers, 225 various types of professional and technical personnel, 19 first-level architects and over 300 sets of medium- and large-sized advanced construction machinery and equipment, with high-grade highways capable of operating over 150km at the same time and 16 Block special large bridge, the output value of more than 3 billion yuan in production capacity.</p>
      <p>The company successively took part in the construction of over 1,200 kilometers of high-grade highways in more than 20 provinces and autonomous regions in China, built more than 130 large and medium-sized bridges and went abroad to participate in the technical assistance and highway and municipal projects in Cameroon. The projects undertaken by the Company have achieved outstanding results in terms of quality and safety, construction schedule and civilized construction. </p><p>The excellent and good project rate reached over 98%, which was praised by construction units and industry authorities. Among them, a number of highly complex projects with complex structures and high technical contents have been completed, such as Fujian's Wuyishan-Shaowu Expressway, Sichuan's Qionglai-Mingshan Expressway, Chongqing Pengshui Hydropower Station's approach road, Yinpan Reservoir, Yalongjiang Jinping I Hydropower Station Xiamen Road extension project, Pingyao Street to Yanshan Expressway and won the Silver Medal of National Quality Project, the First Prize of Quality Project of Ministry of Communications, Sichuan-Chengdu-Jianyang Expressway and Guiyang-Guangzhou Railway Passenger Dedicated Line.</p>
       <p>The company continued to promote scientific and technological progress, with 16 major scientific research successes respectively provincial, ministerial and national awards, of which two reached the international advanced level, won the United States Pittsburgh National Invention Exposition silver and bronze awards.</p>
        <p>Companies adhering to the "integrity-based, boutique based on the market; scientific harmony and innovation cast the enterprise" business philosophy, external to meet the needs of owners as its mission, the pursuit of happiness and sense of accomplishment within the staff to maximize the sense in the whole industry chain , Create value for the community, build quality projects, provide first-class service!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


@endsection