@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))
<div class="section-full p-tb80 bg-white inner-page-padding">
    <div class="container">
        <!-- TITLE START -->
        <div class="section-head">
            <div class="mt-separator-outer separator-left">
                <div class="mt-separator">
                    <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary"></span>Welcome</h2>
                </div>
            </div>
        </div>                   
        <!-- TITLE END -->                 
        <div class="section-content ">
            <div class="row">
                <div class="col-md-5 col-sm-6">
                    <div class="m-about ">
                        <div class="owl-carousel about-us-carousel">
                            <!-- COLUMNS 1 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic8.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic2.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>

                            <!-- COLUMNS 5 -->
                            <div class="item">
                                <div class="ow-img">
                                    <a href="javascript:void(0);"><img src="{{asset('lib/images/gallery/portrait/pic1.jpg')}}" alt="" style="height:358px;width:350px"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                        
                <div class="col-md-7 col-sm-6">
                    <div class="m-about-containt text-black p-t80">
                        <div class="m-about-years bg-moving" style="background-image:url({{asset('lib/images/background/line.png')}});">
                            <span class="text-primary large-title">20</span>
                            <span class="large-title-info">Years of Experience</span>
                        </div>
                        <h3 class="font-weight-600">Improving quality of life with an integrated unified approach.</h3>
                        <p>As a global enterprise engaged in large-scale infrastructure construction and mineral investment business worldwide, 
                            Nigerian Unite One Group Co., Ltd. has been adhering to the management philosophy of casting fine products and first-class tree brand and actively devotes itself to creating more value for customers. 
                            For enterprises to establish Evergreen Foundation, to make outstanding contributions to society. In the process of constant pursuit of value sublimation, 
                            we have established a good relationship of mutual benefit and win-win development with our business partners through compliance and re-commitment and innovation.
                            As always, UNIFI Group adheres to the enterprise tenet of "Honesty, Quality Based on the Market, Scientific Harmony and Innovative Founding Enterprise" and goes hand in hand with friends from all walks of life to move towards the strategic goal of building a leading international engineering contractor!.</p>
                        <div class="author-info p-t20">
                        
                            <div class="author-signature">
                                <img src="{{asset('lib/images/chairman.jpg')}}" alt="chairman" width="150"/>
                            </div>
                            
                            <div class="author-name">
                                <h4 class="m-t0">Zeng You</h4>
                                <p>Chairman</p>
                            </div>
                            <a href="{{route('menu',['name' => 'contact-us'])}}" class="site-button btn-effect m-b15"><span>Get in touch</span></a>                                        
                        </div> 
                    </div>
                </div>                           

            </div>
        </div>
    </div>
</div>   
            <!-- ABOUT COMPANY SECTION END -->
            

@endsection