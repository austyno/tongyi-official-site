@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

  <div class="section-full p-tb80 inner-page-padding">
      <div class="container">
        <div class="section-head">
          <div class="mt-separator-outer separator-left">
          <div class="mt-separator">
              <h2 class="text-black text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Ongoing</span> Projects</h2>
          </div>
        </div>
      </div>

      <!-- GALLERY CONTENT START -->
      <div class="portfolio-wrap mfp-gallery work-grid row clearfix">
        <!-- COLUMNS 1 -->
        @foreach ($ongoinProjects as $op)

          <div class="masonry-item cat-1  col-lg-3 col-md-4 col-sm-6 m-b30">
        <div class="project-classic" style="height:420px">
        <div class="mt-box">
            <div class="mt-thum-bx  img-center-icon  mt-img-overlay2">
                <img src="{{asset($op->image)}}" alt="" style="height:270px;width:360px">
                <div class="overlay-bx">
                    <div class="overlay-icon">
                        <a href="{{route('viewimages',['status' => 'ongoing','slug' => Str::slug($op->name)])}}">
                            <i class="fa fa-external-link mt-icon-box-xs "></i>
                        </a>
                        <a class="mfp-link" href="{{asset($op->image)}}" title="{{ucwords($op->name)}}">
                            <i class="fa fa-arrows-alt mt-icon-box-xs"></i>
                        </a>
                  </div>
                </div>
            </div>
        </div>
        <div class="mt-info p-t20">
            <h4 class="mt-title m-b20 m-t0" style="color:#1d459a"><a href="{{route('viewimages',['status' => 'ongoing','slug' => Str::slug($op->name)])}}">{{ucwords($op->name)}}</a></h4>
              <a href="{{route('viewimages',['status' => 'ongoing','slug' => Str::slug($op->name)])}}" class="site-button-link" data-hover="More Images">More Images &nbsp; [ {{ $op->images->count()}} ]<i class="fa fa-angle-right arrow-animation"></i></a>                               
          </div>
    </div>
</div>
            
        @endforeach
      </div>
      <!-- GALLERY CONTENT END -->
    </div>
  </div>

@endsection