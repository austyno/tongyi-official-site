@extends('layout.main')
@section('content')
@section('crumb', ' >> ' .ucwords($crumb))

<div class="section-full p-tb80 bg-white inner-page-padding">
    <div class="container">
        <!-- TITLE START -->
        <div class="section-head">
            <div class="mt-separator-outer separator-left">
                <div class="mt-separator">
                    <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Road </span>Construction</h2>
                </div>
            </div>
        </div>                   
        <!-- TITLE END -->                 
            <!-- ABOUT COMPANY SECTION START -->
            <div class="section-full p-t80 bg-white" style="margin-top:-30px">
                <div class="container">
                    <div class="section-content ">
                         <div class="m-service-containt text-black">
                         		<div class="row">
                                	<div class="col-md-5 col-sm-12">
                                    	<div class="service-about-left">
                                        	<div class="mt-media">
                                       	    	<img src="{{asset('lib/images/s-1.png')}}" alt=""> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-12">
                                    	<div class="service-about-right m-b30">
                                            <div class="m-about-years bg-moving" style="background-image:url({{asset('lib/images/background/line.png')}});">
                                            <h3 class="m-t0">In our work we have pride, quality is what we provide.</h3>
                                            </div>

                                            <p style="font-size:16px;margin-top:20px">Our projects involve the construction of asphaltic, rigid(concrete) and surface dressed roads, the expansion and rehabilitation of existing roads, infrastructure projects and airport runways in Nasarawa, Benue, Plateau, Ebonyi and various States across Nigeria.</p>
                                            <p style="font-size:16px">To date Tongyi Group has successfully constructed and delivered many Kilometers of new roads and carriage ways, and has rehabilitated many Kilometers of previously constructed roads.</p>
                                            <div class="call-for-quote-outer">
                                            	<div class="call-quote">
                                                	<span>Call For a Quote:</span>
                                                    <h4>Mrs Jiang +2348171966666</h4>
                                                </div>
                                                <div class="call-estimate bg-dark">
                                                <a href="{{route('menu',['name' => 'contact-us'])}}" class="site-button-secondry btn-effect">Get In Touch</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>   
            <!-- ABOUT COMPANY SECTION END -->

    </div>
</div> 

@endsection


