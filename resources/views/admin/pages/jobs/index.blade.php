@extends('admin.layout.main')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>All posted jobs</h3>
        </div>

        <div class="title_right">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <a href="{{route('jobs.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> new job</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>SN</th>
                          <th>Job Title</th>
                          <th>Available Slots</th>
                          <th>Department</th>
                          <th>Qualification</th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($allJobs as $job)
                              
                            <tr >
                            <td>{{ucwords($loop->iteration)}}</td>
                            <td>{{ucwords($job->title)}}</td>
                            <td style="text-align:center">{{ucwords($job->slots)}}</td>
                            <td>{{ucwords($job->department)}}</td>
                            <td>{{ucwords($job->qualification)}}</td>
                            <td style="font-size:20px"><a href="{{route('jobs.edit',['id' => $job->id])}}"><i style="margin-right:5px;color:darkgreen" class="fa fa-edit"></i></a> <a href="{{route('jobs.delete',['id' => $job->id])}}"><i style="color:red" class="fa fa-trash"></i></a></td>
                            <td>{{$job->created_at->format('d M Y')}} &nbsp; {{$job->created_at->diffForHumans()}}</td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection