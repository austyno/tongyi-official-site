@extends('admin.layout.main')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>All recieved applications</h3>
        </div>

        <div class="title_right">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th></th>
                          <th>SN</th>
                          <th>Surename</th>
                          <th>Name</th>
                          <th>Address</th>
                          <th>Email</th>
                          <th>PostalCode</th>
                          <th>country</th>
                          <th>Telephone</th>
                          <th>Specialization</th>
                          <th>Gender</th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($applications as $app)  
                            <tr>
                            <td><a href="{{route('application.delete',['id' => $app->id])}}"><i class="fa fa-close" style="font-size:15px;color:red"></i></a></td>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ucwords($app->surename)}}</td>
                            <td>{{ucwords($app->name)}}</td>
                            <td>{{ucwords($app->address)}}</td>
                            <td>{{$app->email}}</td>
                            <td>{{$app->postalCode}}</td>
                            <td>{{ucwords($app->country)}}</td>
                            <td>{{$app->tel}}</td>
                            <td>{{ucwords($app->specialization)}}</td>
                            <td>{{ucwords($app->gender)}}</td>
                            <td><button id="letter" class="btn btn-default btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg" data-id="{{$app->id}}">Cover Letter</button></td>
                            <td><a href="{{route('resume',['id' => $app->id])}}" class="btn btn-default btn-sm">Resume</a></td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="name"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="" style="padding:12px">
                    <p id="content" style="text-align:justify">
                        <img id="preloader" src="{{asset('lib/images/preloader.gif')}}" style="margin-left:40%"/>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

        </div>
    </div>
</div>
@endsection