@extends('admin.layout.main')
@section('content')
<div class="x_panel">
  <div class="x_title">
  <h2>Edit Job: &nbsp; {{$editJob->title}}<small></small></h2>
  <a href="{{route('jobs.all')}}" type="button" class="btn btn-primary pull-right">Cancel</a>

    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <br>
    <form class="form-horizontal form-label-left" method="post" action="{{route('jobs.update',['id' => $editJob->id])}}" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Job Title<span class="required">*</span>
        </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="title" value="{{$editJob->title}}" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div><br>

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slots">Available slots<span class="required">*</span>
        </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="number" name="slots" value="{{$editJob->slots}}" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div><br>

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qualification">Qualification Required<span class="required">*</span>
        </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="qualification" value="{{$editJob->qualification}}" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div><br>

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Department">Department<span class="required">*</span>
        </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="department" value="{{$editJob->department}}" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div><br>

    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="submit" class="btn btn-success">Update</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection
