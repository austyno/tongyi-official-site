@extends('admin.layout.main')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><small>Upload images for:</small> &NonBreakingSpace; {{$name}}</h3>
        </div>

        <div class="title_right">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add images</h2>
                    <a href="{{url()->previous()}}" class="btn btn-info pull-right">Back</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                   <p>Drag multiple files to the box below for multi upload or click to select images as many required.</p>
                <form id="dropzone" action="{{route('ongoing.upload',['id' => $id,'status' => $status])}}" class="dropzone" enctype="multipart/form-data">
                    {{csrf_field()}}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection