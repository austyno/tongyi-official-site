@extends('admin.layout.main')
@section('content')
<div class="x_panel">
  <div class="x_title">
  <h2>Edit: {{$edit->name}}<small></small></h2>
      <a href="{{route('completed.all')}}" type="button" class="btn btn-primary pull-right">Cancel</a>

    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <br>
    <form class="form-horizontal form-label-left" method="post" action="{{route('completed.update',['id' => $edit->id])}}" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Project Name<span class="required">*</span>
        </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" name="name" value="{{$edit->name}}" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div><br>

    <input type="hidden" name="status" value="{{$edit->status}}">

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="News-Image">Cover Image<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <img id="pre" src="{{asset($edit->image)}}" class="image-responsive" height="150" width="200" style="" data-toggle="tooltip" title="image preview" />
          <input type="file" name="image" accept="image/*" class="form-control col-md-7 col-xs-12" 
            onchange="document.getElementById('pre').src = window.URL.createObjectURL(this.files[0]); 
            document.getElementById('pre').style.display ='inline-block'
          ">
      </div>
    </div><br>


    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="submit" class="btn btn-success">Update</button>
      </div>
    </div>
    </form>
  </div>
</div>

@endsection
