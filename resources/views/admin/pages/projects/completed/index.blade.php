@extends('admin.layout.main')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>All Completed Projects</h3>
        </div>

        <div class="title_right">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <a href="{{route('completed.newProject')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> new project</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        To view project images click on the arrow 
                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>SN</th>
                          <th>Project Name</th>
                          <th>Images</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($projects as $pro)
                              
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td style="font-weight:bold">{{$pro->name}}</td>
                            <td><a href="{{route('completed.images',['id' => $pro->id])}}" style="font-size:17px">{{$pro->images->count()}} &nbsp;<i class="fa fa-angle-right"></i></a></td>
                            <td>
                                <a href="{{route('completed.edit',['id' => $pro->id])}}"><i class="fa fa-edit" style="font-size:17px"></i></a>
                                <a href="{{route('completed.del',['id' => $pro->id])}}" style="float:right"><i class="fa fa-trash" style="font-size:17px;color:red"></i></a>
                            </td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection