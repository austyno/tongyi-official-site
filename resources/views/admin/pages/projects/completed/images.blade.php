@extends('admin.layout.main')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            @if (isset($images[0]->project->name))
            <h3>{{$images[0]->project->name}} </h3>
                
            @endif
        </div>

        <div class="title_right">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <a class="btn btn-info" href="{{ route('completed.all') }}">Back</a>
                <a class="btn btn-primary pull-right" href="{{route('completed.newImages',['proId' => $images[0]->project->id,'status'=> $images[0]->project->status, 'name' => $images[0]->project->name])}}"><i class="fa fa-plus"></i> Add Images</a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row mfp-gallery">
                      <p>Use the add images button to add images to this project</p>
                      @foreach ($images as $img)
                        <div class="col-md-55">
                            <div class="thumbnail">
                            <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="{{asset($img->image)}}" alt="image" />
                                <div class="mask" style="height:100%">
                                <p></p>
                                <div class="tools tools-bottom">
                                    <a class="mfp-link" href="{{asset($img->image)}}" title="{{$img->project->name}}"><i class="fa fa-eye" data-toggle="tooltip" title="view image"></i></a>
                                    <a href="#" data-toggle="tooltip" title="delete image"><i id="remove" class="fa fa-times" data-id="{{$img->id}}" style="color:red"></i></a>
                                </div>
                                </div>
                            </div>
                            <div class="caption">
                            <p>{{$img->project->name}}</p>
                            </div>
                            </div>
                        </div>
                    @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
    <div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"></h4>
    </div>
    <div class="modal-body">
        <h4>Are you sure you want to delete image Number <span id="imgNo"></span></h4>
        <p></p>
        <p><input id="imgId" type="hidden" value="#"></p>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
        <button type="button" class="btn btn-danger" id="cnt">YES</button>
    </div>

    </div>
</div>
</div>

@endsection