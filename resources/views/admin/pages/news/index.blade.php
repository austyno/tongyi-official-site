@extends('admin.layout.main')
@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> News Items [ {{$news->count()}} ]</h3>
                <small>Click on published to unpublishe and vise versa</small>
              </div>
            <a href="{{route('news.create')}}" class="btn btn-large btn-success pull-right"><i class="fa fa-plus"></i></a>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                    <div class="row">
                      @foreach ($news as $item)
                          
                      <div class="col-md-55">
                        <div class="thumbnail" style="height:auto">
                          <div class="image view view-first">
                          <img style="width: 100%; display: block;" src="{{asset($item->image)}}" alt="image" style="width:167px;height:100%" />
                            <div class="mask" style="height:100%">
                              <div class="tools tools-bottom">
                              <a href="{{route('news.show',['slug' => $item->slug])}}"><i class="fa fa-eye" data-toggle="tooltip" title="view news"></i>
                                <a href="{{route('news.edit',['id' => $item->id])}}"><i class="fa fa-pencil" data-toggle="tooltip" title="edit news"></i></a>
                                <a href="#" ><i id="del" class="fa fa-times" data-id="{{$item->id}}" data-title="{{$item->title}}" style="color:red" data-toggle="tooltip" title="delete news"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption" style="height:130px">
                            <p><strong>Title: </strong>{{$item->title}}.</p>
                          <a href="{{ route('news.published.toggle',['status' => $item->published,'id' => $item->id])}}"><p><strong>Published: </strong>{{$item->published}}</p></a>
                          <p><strong>Date Published: </strong> {{$item->created_at->format('d M Y')}} [ {{$item->created_at->diffForHumans()}} ]</p>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <h4 class="modal-title" id="myModalLabel" style="color:red"> Delete News Item</h4> 
          </div>
         <div class="modal-body">
           <p id="msg" style="color:red;font-size:20">Are you sure you want to delete</p>
            <p id="title" style="font-size:18px;font-style:bold"></p>
            <input type="hidden" id="hidden">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
            <button type="button" class="btn btn-danger" id="yes">YES</button>
         </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>
@endsection