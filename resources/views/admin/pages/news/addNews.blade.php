@extends('admin.layout.main')
@section('content')
<div class="x_panel">
  <div class="x_title">
    <h2>Create news<small></small></h2>
      <a href="{{route('news')}}" type="button" class="btn btn-primary pull-right">Cancel</a>

    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <br>
    <form class="form-horizontal form-label-left" method="post" action="{{route('news.store')}}" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title<span class="required">*</span>
        </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="title" value="{{old('title')}}" required="required" class="form-control col-md-7 col-xs-12">
      </div>
    </div><br>

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="News-Image">News Image<span class="required">*</span>
      </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
          <img id="pre" src="#" class="image-responsive" height="150" width="200" style="display:none" data-toggle="tooltip" title="image preview" />
          <input type="file" name="image" accept="image/*" class="form-control col-md-7 col-xs-12" 
            onchange="document.getElementById('pre').src = window.URL.createObjectURL(this.files[0]); 
                    document.getElementById('pre').style.display ='inline-block'
          ">
      </div>
    </div><br>

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Publish Now<span class="required">*</span>
        </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <p>
          Yes: &nbsp; <input type="radio" name="publish" value="YES" checked="" required /> 
          &nbsp;&nbsp;&nbsp;&nbsp;
          NO: &nbsp; <input type="radio" name="publish" value="NO" />
        </p>
      </div>
    </div><br>

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Content">Content<span class="required">*</span>
        </label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea name="content" required="required" class="form-control col-md-7 col-xs-12" rows="10"></textarea>
      </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
      <a href="{{route('news')}}" type="button" class="btn btn-primary">Cancel</a>
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
    </form>
  </div>
</div>
  
  
@endsection