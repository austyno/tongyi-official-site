@extends('admin.layout.main')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
        <h3></h3>
        </div>

        <div class="title_right">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
            <a href="{{route('news')}}" class="btn btn-primary pull-right">Back</a>
            <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <img src="{{asset($item[0]->image)}}" alt="" class="" style="width:100%">
                </div><br>
                <div class="row">
                <div class="" style="display:inline;margin-bottom:40px">
                    <span class="label label-default" style="margin:20px">Published: &nbsp;{{$item[0]->published}}</span>
                    <span class="label label-default" style="">On: &nbsp;{{$item[0]->created_at->format('d M Y')}}</span>
                </div>
                </div><br>
                <h2 style="font-weight:bold;font-size:20px">{{$item[0]->title}}</h2>
                <div class="">
                    <p style="font-size:16px;line-height:25px;text-align:justify">{{$item[0]->content}}</p>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection