@extends('admin.layout.main')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>All recieved Enquiries</h3>
        </div>

        <div class="title_right">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        Newer Enquiries are at the top
                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>SN</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Message</th>
                          <th>Date Received</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($enquiries as $enq)  
                            <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$enq->name}}</td>
                            <td>{{$enq->email}}</td>
                            <td>{{$enq->phone}}</td>
                            <td><button 
                                id="btn"
                                class="btn btn-xs btn-default" 
                                data-toggle="modal" 
                                data-target=".bs-example-modal-lg" 
                                data-id="{{$enq->id}}">
                                read massege
                                </button>
                            </td>
                        <td>{{$enq->created_at->diffForHumans()}}&nbsp; {{$enq->created_at->format('M d Y')}}</td>
                            <td><a href="{{route('enquire.del',['id' => $enq->id])}}"><i class="fa fa-trash" style="font-size:18px;color:red"></i></a></td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="name"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="" style="padding:12px">
                    <p id="content" style="text-align:justify">
                    <img src="{{asset('lib/images/preloader.gif')}}" style="margin-left:40%"/>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

        </div>
    </div>
</div>
@endsection