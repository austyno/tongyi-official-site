
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tonyi Admin </title>

    <!-- Bootstrap -->
    <link href="{{asset('admin/lib/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('admin/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('admin/lib/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('admin/lib/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- Datatables -->
    <link href="{{asset('admin/lib/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/switchery/dist/switchery.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/css/magnific-popup.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/lib/dropzone/dist/min/dropzone.min.css')}}" rel="stylesheet">



    <!-- Custom Theme Style -->
    <link href="{{asset('admin/lib/css/custom.min.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/lib/css/toastr.min.css') }}" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fa fa-dashboard"></i> <span>Site Manager</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
              <img src="{{asset('lib/images/placeholder.png')}}" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
              <h2>{{Auth::user()->name}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                <li><a href="{{route('home')}}"><i class="fa fa-home"></i> Home </a>
                  </li>
                  <li><a><i class="fa fa-edit"></i> News <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('news')}}">All News</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-road"></i> Projects <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('ongoing.all')}}">Ongoing Projects</a></li>
                    <li><a href="{{route('completed.all')}}">Completed Projects</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-briefcase"></i>Jobs <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{route('jobs.all')}}">Vacancies</a></li>
                    <li><a href="{{route('applications.all')}}">Applications</a></li>
                    </ul>
                  </li>
                <li><a href="{{route('enquire.all')}}"><i class="fa fa-gavel"></i>Enquiries</a></li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">{{Auth::user()->name}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li>
                    <a href="{{ route('logout') }}" 
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out pull-right"></i> 
                          Log Out
                    </a>
                  </li>
                      <form id="logout-form" method="post" action="{{ route('logout') }}" style="display:none">
                        {{ csrf_field() }}
                      </form>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Tongyi Admin Panel
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('admin/lib/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('admin/lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('admin/lib/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('admin/lib/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('admin/lib/moment/min/moment.min.js')}}"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Datatables -->
    <script src="{{asset('admin/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('admin/lib/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    {{-- <script src="{{asset('admin/lib/switchery/dist/switchery.min.js')}}"></script> --}}
    <script src="{{ asset('lib/js/magnific-popup.min.js')}}"></script><!-- MAGNIFIC-POPUP JS -->
    <script src="{{asset('admin/lib/dropzone/dist/min/dropzone.min.js')}}"></script>
    <script>
    window.Dropzone.options.dropzone= {
        maxFilesize: 1,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        timeout: 5000,
        success: function(file,response){

            console.log(response);
        },
        error: function(file,response){
            return false;
        }
    };
</script>
    
    <!-- Custom Theme Scripts -->
    <script src="{{asset('admin/lib/js/custom.min.js')}}"></script>
    <script src="{{ asset('admin/lib/js/gall.js')}}"></script><!-- CUSTOM FUCTIONS to initialize magnific popup  -->

    <script>
      $(document).ready(function(){
        $('[data-tool="tooltip"]').tooltip(); 
      });
    </script>
    <script src="{{asset('admin/lib/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        tinymce.init({
            // selector: 'textarea',
            plugins : 'advlist autolink link image lists print preview emoticons spellchecker',
            spellchecker_language: 'en',
            valid_elements: "a[href|target=_blank],strong/b,div[align],br,p"
        });
    
    </script>
    <script src=" {{ asset('admin/lib/js/toastr.min.js')}}"></script>
  <script>

      toastr.options = {
      "positionClass": "toast-top-right"
    }
    
      @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}")
      @endif

      @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}")
      @endif

    </script>
    <script>
      $(document).ready(function(){
        $(document).on('click','#del', function(e){

          // alert('clicked')
            e.preventDefault();

            var id = $(this).data('id');
            var title = $(this).data('title');
          

            // $('#myModalLabel').html(id);
            $('#title').html(title);
            $('#hidden').val(id)
            $('#myModal').modal('show')
        })

          $(document).on('click','#yes',function(){

            var id = $('#hidden').val();
            var token = "{{csrf_token()}}"
            var url = '/delete'

            $('#myModal').modal('hide')


            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'html',
                data: {
                    id: id,
                    _token: token
                  }
                
            })
            .done(function(data){
              data=JSON.parse(data)

              if(data == 'successful'){
                window.location = "{{route('news')}}"
              }

              // console.log(data)

            })

          });

          $(document).on('click','#remove',function(){

            var id = $(this).data('id');

            $('.bs-example-modal-sm').modal('show')

            $('#imgId').val(id)
            $('#imgNo').html(id)



            $(document).on('click','#cnt',function(){

            var token ="{{csrf_token()}}"
            var imgNo = $('#imgId').val()
            var url = '/deleteImg'

            $('.bs-example-modal-sm').modal('hide')


            $.ajax({
              url: url,
              type: 'POST',
              dataType: 'html',
              data:{
                id: id,
                _token: token
              }
            })
            .done(function(data){
              data = JSON.parse(data)
              console.log(data)

              if(data == 'successful'){
                window.location = "{{ url()->previous() }}"
              }

            })


            })

          })

          //cover letter
          $(document).on('click','#letter',function(){

            var id = $(this).data('id');
            var url = "/letter";
            var token ="{{csrf_token()}}"

            $.ajax({
              url: url,
              type: 'POST',
              dataType: 'html',
              data: {
                id: id,
                _token: token
              }

            }).done(function(data){
              data = JSON.parse(data)
              $('#content').html(data.coverLetter)
              $('#name').html(data.name)
            })
          });

          //enquiries
          $(document).on('click','#btn',function(){
            var id = $(this).data('id');
            var url = '/messages';
            
            $.ajax({
              url: url,
              type: 'GET',
              dataType: 'html',
              data: {
                id: id
              }
            })
            .done(function(data){
              data = JSON.parse(data)
              $('#name').html(data.name);
              $('#content').html(data.message)
              $('#preloader').hide()
              console.log(data);
            })
          });
          
      })
    </script>
  </body>
</html>