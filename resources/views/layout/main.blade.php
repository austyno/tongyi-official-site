<!DOCTYPE html>
<html lang="en">

<head>

  <!-- META -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keywords" content="" />
  <meta name="author" content="" />
  <meta name="robots" content="" />
  <meta name="description" content="" />

  <!-- FAVICONS ICON -->
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

  <!-- PAGE TITLE HERE -->
  <title>TONGYI</title>

  <!-- MOBILE SPECIFIC -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->

  <!-- BOOTSTRAP STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/bootstrap.min.css')}}">

  <!-- FONTAWESOME STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/fontawesome/css/font-awesome.min.css')}}" />

  <!-- OWL CAROUSEL STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/owl.carousel.min.css')}}">

  <!-- MAGNIFIC POPUP STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/magnific-popup.min.css')}}">

  <!-- LOADER STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/loader.min.css')}}">

  <!-- FLATICON STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/flaticon.min.css')}}">

  <!-- MAIN STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/style.css')}}">

  <!-- Color Theme Change Css -->
  <link rel="stylesheet" class="skin" type="text/css" href="{{ asset('lib/css/skin/skin-8.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/switcher.css')}}">

  <!-- REVOLUTION SLIDER CSS -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/plugins/revolution/revolution/css/settings.css')}}">

  <!-- REVOLUTION NAVIGATION STYLE -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/plugins/revolution/revolution/css/navigation.css')}}">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
    rel="stylesheet">
  <link
    href="https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

    <link href="{{ asset('admin/lib/css/toastr.min.css') }}" rel="stylesheet">
</head>
<body>
  <div class="page-wraper">

    <!-- HEADER START -->
    <header class="site-header header-style-1">
      <div class="top-bar bg-gray">
        <div class="container">
          <div class="row">
            <div class="mt-topbar-left clearfix">
              <ul class="list-unstyled e-p-bx pull-right">
                <li><i class="fa fa-envelope"></i>info@tongyigroupintl.com</li>
                <li><i class="fa fa-phone"></i>+2348171966666</li>
                <li><i class="fa fa-clock-o"></i>Sun-Sat 9.45 am</li>
              </ul>
            </div>
            <div class="mt-topbar-right clearfix">
              <div class="appint-btn"><a href="#" class="site-button">Make an Appointment</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="sticky-header main-bar-wraper">
        <div class="main-bar bg-white">
          <div class="container">
            <div class="logo-header" style="width:300px">
              <div class="logo-header-inner logo-header-one" >
              <a href="{{route('welcome')}}">
                  <img src="{{ asset('lib/images/logo.jpg')}}" alt="" style="height:75px;"/>
                    <span style="font-size:21px;color:#1D459A">TONGYI GROUP LTD</span>
                </a>
              </div>
            </div>
            <!-- NAV Toggle Button -->
            <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- ETRA Nav -->
            <div class="extra-nav">
              <div class="extra-cell">
                <a href="#search">
                  <span>中文</span>
                </a>
              </div>
              <div class="extra-cell">
                <a href="#" class="contact-slide-show"> info &nbsp;<i class="fa fa-angle-left arrow-animation"></i></a>
              </div>
            </div>
            <!-- ETRA Nav -->

            <!-- Contact Nav -->
            <div class="contact-slide-hide " style="background-image:url(lib/images/background/bg-5.png)">
              <div class="contact-nav">
                <a href="javascript:void(0)" class="contact_close">&times;</a>
                <div class="contact-nav-form p-a30">
                  <div class="contact-info   m-b30">

                    <div class="mt-icon-box-wraper center p-b30">
                      <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-phone"></i></div>
                      <div class="icon-content">
                        <h5 class="m-t0 font-weight-500">Phone number</h5>
                        <p>+2348171966666</p>
                      </div>
                    </div>

                    <div class="mt-icon-box-wraper center p-b30">
                      <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-envelope"></i></div>
                      <div class="icon-content">
                        <h5 class="m-t0 font-weight-500">Email address</h5>
                        <p>info@tongyigroupintl.com</p>
                      </div>
                    </div>

                    <div class="mt-icon-box-wraper center p-b30">
                      <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-map-marker"></i></div>
                      <div class="icon-content">
                        <h5 class="m-t0 font-weight-500">Address info</h5>
                        <p>Plot 105，Ebitu Ukiwe Street，Jabi，Abuja，Nigeria</p>
                      </div>
                    </div>
                  </div>
                  <div class="full-social-bg">
                    <ul>
                      <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#" class="google"><i class="fa fa-google"></i></a></li>
                      <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                      <li><a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a></li>
                      <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                  </div>
                  <div class="text-center">
                    <h4 class="font-weight-600">&copy;TONGYI Ltd</h4>
                  </div>
                </div>
              </div>
            </div>
            <!-- SITE Search -->
            <!-- MAIN Vav -->
            <div class="header-nav navbar-collapse collapse">
              <ul class=" nav navbar-nav">
                <li>
                <a href="{{ route('welcome')}}">Home</a>
                </li>
                @foreach ($menus as $menu)
                    <li>
                      <a href="">{{$menu->name}}</a>
                      <ul class="sub-menu">
                        @foreach ($menu->submenus as $sub)
                            <li>
                            <a href="{{route('menu',['name' => Str::slug($sub->name)])}}">{{ucwords($sub->name)}}</a>
                            </li>
                        @endforeach
                      </ul>
                    </li>
                @endforeach
              </ul>
            </div>

          </div>
        </div>
      </div>
    </header>
    <!-- HEADER END -->
    <div class="page-content">
      <!-- INNER PAGE BANNER -->
      <div class="mt-bnr-inr overlay-wraper bg-parallax bg-top-center" data-stellar-background-ratio="0.5"
        style="background-image:url({{asset('lib/images/banner/1.jpg')}});">
        <div class="overlay-main bg-black opacity-07"></div>
        <div class="container">
          <div class="mt-bnr-inr-entry">
            <div class="banner-title-outer">
              <div class="banner-title-name">
                <h2 class="m-b0">Creating quality urban lifestyles, building stronger communities.</h2>
              </div>
            </div>
            <!-- BREADCRUMB ROW -->
            <div>
              <ul class="mt-breadcrumb breadcrumb-style-1">
                <li><a href="{{route('welcome')}}">Home</a></li>
                <li>@yield('crumb')</li>
              </ul>
            </div>

            <!-- BREADCRUMB ROW END -->
          </div>
        </div>
      </div>
      <!-- INNER PAGE BANNER END -->

      @yield('content')
      
    </div>
    <footer class="site-footer footer-large  footer-dark	footer-wide">
      <div class="container call-to-action-wrap bg-no-repeat bg-center"
        style="background-image:url(lib/images/background/bg-site.png);">
        <div class="p-a30 bg-primary">
          <p style="font-size:18px;line-height:30px;text-align:center">We let our quality work and commitment to customer satisfaction be our slogan.<br> <span style="text-align: center">Quality you deserve and dependability you can count on.</span><br> <span style="text-align: center">Call Now Mrs Jiang +2348171966666</span></p>

        </div>
      </div>
      <!-- FOOTER BLOCKES START -->
      <div class="footer-top overlay-wraper">
        <div class="overlay-main"></div>
        <div class="container">
          <div class="row">
            <!-- ABOUT COMPANY -->
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="widget widget_about">
                <div class="logo-footer clearfix p-b15">
                <a href="{{route('welcome')}}"><img src="{{asset('lib/images/bgoff.jpg')}}" alt="" style="height:60px">
                </a>
                <span style="margin-left:;font-size:20px">TONGYI GROUP LTD</span>
                </div>
                <p class="max-w400">Today we can tell you, thanks to your passion, hard work creativity, and expertise,
                  you delivered us the most beautiful house great looks.</p>

                  

                <ul class="social-icons  mt-social-links">
                  <li><a href="javascript:void(0);" class="fa fa-google"></a></li>
                  <li><a href="javascript:void(0);" class="fa fa-rss"></a></li>
                  <li><a href="javascript:void(0);" class="fa fa-facebook"></a></li>
                  <li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
                  <li><a href="javascript:void(0);" class="fa fa-linkedin"></a></li>
                </ul>
              </div>
            </div>

            <!-- RESENT POST -->
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="widget widget_address_outer">
                <h4 class="widget-title">Contact Us</h4>
                <ul class="widget_address">
                  <li>Plot 105，Ebitu Ukiwe Street，Jabi，Abuja，Nigeria</li>
                  <li>+2348171966666</li>
                </ul>

              </div>
            </div>

            <!-- USEFUL LINKS -->
            <div class="col-lg-3 col-md-6 col-sm-6 footer-col-3">
              <div class="widget widget_services inline-links">
                <h4 class="widget-title">Useful links</h4>
                <ul>
                  <li><a href="{{route('menu',['name' => 'group-introduction'])}}">About</a></li>
                  <li><a href="{{route('menu',['name' => 'completed-projects'])}}">Projects</a></li>
                  <li><a href="{{route('menu',['name' => 'group-news'])}}">News</a></li>
                  <li><a href="{{route('menu',['name' => 'contact-us'])}}">Contact Us</a></li>
                </ul>
              </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="widget recent-posts-entry-date">
                <h4 class="widget-title">Recent News</h4>
                <div class="widget-post-bx">

                  @foreach ($footerNews as $fn)
                    <div class="bdr-light-blue widget-post clearfix  bdr-b-1 m-b10 p-b10">
                    <div class="mt-post-date text-center text-uppercase text-white p-tb5">
                    <strong class="p-date">{{$fn->created_at->format('d')}}</strong>
                    <span class="p-month">{{$fn->created_at->format('M')}}</span>
                    <span class="p-year">{{$fn->created_at->format('Y')}}</span>
                    </div>
                    <div class="mt-post-info">
                      <div class="mt-post-header">
                        <h6 class="post-title"><a href="{{route('readmore',['slug' => $fn->slug])}}">{{$fn->title}}</a>
                        </h6>
                      </div>
                      <div class="mt-post-meta">
                        <ul>
                          <li class="post-author"><i class="fa fa-user"></i>By Admin</li>
                        </ul>
                      </div>
                    </div>
                  </div>  
                  @endforeach

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- FOOTER COPYRIGHT -->
      <div class="footer-bottom overlay-wraper">
        <div class="overlay-main"></div>
        <div class="container">
          <div class="row">
            <div class="mt-footer-bot-center">
              <span class="copyrights-text">© 2019 All rights reserved </span>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <script src="{{ asset('lib/js/jquery-1.12.4.min.js')}}"></script><!-- JQUERY.MIN JS -->
  <script src="{{ asset('lib/js/bootstrap.min.js')}}"></script><!-- BOOTSTRAP.MIN JS -->

  <script src="{{ asset('lib/js/magnific-popup.min.js')}}"></script><!-- MAGNIFIC-POPUP JS -->

  <script src="{{ asset('lib/js/waypoints.min.js')}}"></script><!-- WAYPOINTS JS -->
  <script src="{{ asset('lib/js/counterup.min.js')}}"></script><!-- COUNTERUP JS -->
  <script src="{{ asset('lib/js/waypoints-sticky.min.js')}}"></script><!-- COUNTERUP JS -->

  <script src="{{ asset('lib/js/isotope.pkgd.min.js')}}"></script><!-- MASONRY  -->

  <script src="{{ asset('lib/js/owl.carousel.min.js')}}"></script><!-- OWL  SLIDER  -->
  <script src="{{ asset('lib/js/jquery.owl-filter.js')}}"></script>

  <script src="{{ asset('lib/js/stellar.min.js')}}"></script><!-- PARALLAX BG IMAGE   -->



  <script src="{{ asset('lib/js/custom.js')}}"></script><!-- CUSTOM FUCTIONS  -->
  <script src="{{ asset('lib/js/shortcode.js')}}"></script><!-- SHORTCODE FUCTIONS  -->
  <script src="{{ asset('lib/js/jquery.bgscroll.js')}}"></script><!-- BACKGROUND SCROLL -->
  <script src="{{ asset('lib/js/switcher.js')}}"></script><!-- SWITCHER FUCTIONS  -->
  <!-- REVOLUTION JS FILES -->

  <script src="{{ asset('lib/plugins/revolution/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
  <script src="{{ asset('lib/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>

  <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
  <script src="{{ asset('lib/plugins/revolution/revolution/js/extensions/revolution-plugin.js')}}"></script>

  <!-- REVOLUTION SLIDER SCRIPT FILES -->
  <script src="{{ asset('lib/js/rev-script-1.js')}}"></script>
  <script src=" {{ asset('admin/lib/js/toastr.min.js')}}"></script>
  <script>

      toastr.options = {
      "positionClass": "toast-top-right"
    }
    
      @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}")
      @endif

      @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}")
      @endif

    </script>
</body>
</html>