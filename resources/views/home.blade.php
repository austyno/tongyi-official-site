@extends('admin.layout.main')
@section('content')
<style>
  .tile-stats:hover{
    box-shadow: 0 5px 5px 5px rgba(0, 0, 0, 0.5);
  }
  .tile-stats{
    border-top: 10px solid #2a3f54; 
    border-left:1.5px solid rgba(0, 0, 0, 0.5);
    border-right:1.5px solid #2a3f54;
    border-bottom:1.5px solid #2a3f54;
    height:200px;
    width:300px;
    margin:10px;
  }
  .col-lg-4{
    height:200px;
    width:300px;
    margin:10px;
  }
  .count{
    margin-left:25px;
    margin-top: 20px;
    text-align:center;
    width:120px;

  }
  .icon{
  }
  .name{
    margin-left:20px;
    margin-top:25%;
  }
</style>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Dashboard</h3>
        </div>

        <div class="title_right">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Everything at a glance</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="row">
                      <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats" >
                          <div class="icon">
                            <a href="{{route('news')}}">
                              <i class="fa fa-bell-o"></i>
                            </a>
                          </div>
                        <div class="count" >{{$publishedNews->count()}}</div>
                        <div class="name">
                          <h3 class=""><a href="{{route('news')}}">Published News</a></h3>
                        </div>
                          {{-- <p></p> --}}
                        </div>
                      </div>

                      <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats" style="">
                          <div class="icon">
                            <a href="{{route('news')}}">
                              <i class="fa fa-bell-o"></i>
                            </a>
                          </div>
                        <div class="count" >{{$unpublished->count()}}</div>
                          <div class="name">
                            <h3><a href="{{route('news')}}" >Unpublished News</a></h3>
                          </div>
                        </div>
                      </div>

                      <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-road"></i>
                          </div>
                        <div class="count" >{{$completedProjects->count()}}</div>
                          <div class="name">
                            <h3><a href="{{route('completed.all')}}" >completed Projects</a></h3>
                          </div>
                        </div>
                      </div>

                      <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-bank"></i>
                          </div>
                        <div class="count" >{{$ongoingProjects->count()}}</div>
                          <div class="name">
                              <h3><a href="{{route('ongoing.all')}}" >Ongoing Projects</a></h3>
                          </div>
                        </div>
                      </div>



                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12" >
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-graduation-cap"></i>
                          </div>
                        <div class="count" >{{$applications->count()}}</div>
                          <div class="name">
                            <h3><a href="{{route('applications.all')}}" >Applications Recieved</a></h3>
                          </div>
                        </div>
                      </div>

                      <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-suitcase"></i>
                          </div>
                        <div class="count" >{{$vacancies->count()}}</div>
                          <div class="name">
                            <h3><a href="{{route('jobs.all')}}" >Posted Vacancies</a></h3>
                          </div>
                        </div>
                      </div>

                      <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-gavel"></i>
                          </div>
                        <div class="count" >{{$enquiries->count()}}</div>
                          <div class="name">
                            <h3><a href="{{ route('enquire.all')}}" >Enquiries Recieved</a></h3>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection