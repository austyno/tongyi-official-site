<!DOCTYPE html>
<html lang="en">

<head>

  <!-- META -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keywords" content="" />
  <meta name="author" content="" />
  <meta name="robots" content="" />
  <meta name="description" content="" />

  <!-- FAVICONS ICON -->
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

  <!-- PAGE TITLE HERE -->
  <title>Tongyi Group</title>

  <!-- MOBILE SPECIFIC -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->

  <!-- BOOTSTRAP STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/bootstrap.min.css')}}">

  <!-- FONTAWESOME STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/fontawesome/css/font-awesome.min.css')}}" />

  <!-- OWL CAROUSEL STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/owl.carousel.min.css')}}">

  <!-- MAGNIFIC POPUP STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/magnific-popup.min.css')}}">

  <!-- LOADER STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/loader.min.css')}}">

  <!-- FLATICON STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/flaticon.min.css')}}">

  <!-- MAIN STYLE SHEET -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/style.css')}}">

  <!-- Color Theme Change Css -->
  <link rel="stylesheet" class="skin" type="text/css" href="{{ asset('lib/css/skin/skin-8.css')}}">


  <!-- REVOLUTION SLIDER CSS -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/plugins/revolution/revolution/css/settings.css')}}">

  <!-- REVOLUTION NAVIGATION STYLE -->
  <link rel="stylesheet" type="text/css" href="{{ asset('lib/plugins/revolution/revolution/css/navigation.css')}}">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
    rel="stylesheet">
  <link
    href="https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

</head>

<body>

  <div class="page-wraper">

    <!-- HEADER START -->
    <header class="site-header header-style-1">
      <div class="top-bar bg-gray">
        <div class="container">
          <div class="row">
            <div class="mt-topbar-left clearfix">
              <ul class="list-unstyled e-p-bx pull-right">
                <li><i class="fa fa-envelope"></i>tongyi@tongyi.com</li>
                <li><i class="fa fa-phone"></i>+2348171966666</li>
                <li><i class="fa fa-clock-o"></i>Sun-Sat 9.45 am</li>
              </ul>
            </div>
            <div class="mt-topbar-right clearfix">
            <div class="appint-btn"><a href="{{route('menu',['name' => 'contact-us'])}}" class="site-button">Book An Appointment</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="sticky-header main-bar-wraper">
        <div class="main-bar bg-white">
          <div class="container">
            <div class="logo-header" style="width:300px">
              <div class="logo-header-inner logo-header-one" >
              <a href="{{route('welcome')}}">
                  <img src="{{ asset('lib/images/logo.jpg')}}" alt="" style="height:75px;"/>
                    <span style="font-size:21px;color:#1D459A">TONGYI GROUP LTD</span>
                </a>
              </div>
            </div>
            <!-- NAV Toggle Button -->
            <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- ETRA Nav -->
            <div class="extra-nav">
              <div class="extra-cell">
                <a href="#search">
                  <span>中文</span>
                </a>
              </div>
              <div class="extra-cell">
                <a href="#" class="contact-slide-show">info<i class="fa fa-angle-left arrow-animation"></i></a>
              </div>
            </div>
            <!-- ETRA Nav -->

            <!-- Contact Nav -->
            <div class="contact-slide-hide " style="background-image:url(lib/images/background/bg-5.png)">
              <div class="contact-nav">
                <a href="javascript:void(0)" class="contact_close">&times;</a>
                <div class="contact-nav-form p-a30">
                  <div class="contact-info   m-b30">

                    <div class="mt-icon-box-wraper center p-b30">
                      <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-phone"></i></div>
                      <div class="icon-content">
                        <h5 class="m-t0 font-weight-500">Phone number</h5>
                        <p>+2348171966666</p>
                      </div>
                    </div>

                    <div class="mt-icon-box-wraper center p-b30">
                      <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-envelope"></i></div>
                      <div class="icon-content">
                        <h5 class="m-t0 font-weight-500">Email address</h5>
                        <p>info@tongyigroupintl.com</p>
                      </div>
                    </div>

                    <div class="mt-icon-box-wraper center p-b30">
                      <div class="icon-xs m-b20 scale-in-center"><i class="fa fa-map-marker"></i></div>
                      <div class="icon-content">
                        <h5 class="m-t0 font-weight-500">Address info</h5>
                        <p>Plot 105 Ebitu Ukiwe Street，Jabi，Abuja，Nigeria</p>
                      </div>
                    </div>
                  </div>
                  <div class="full-social-bg">
                    <ul>
                      {{-- <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#" class="google"><i class="fa fa-google"></i></a></li>
                      <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                      <li><a href="#" class="tumblr"><i class="fa fa-tumblr"></i></a></li> --}}
                      <li><a href="https://www.twitter.con/tongyigroup" class="twitter"><i class="fa fa-twitter"></i></a></li>
                      {{-- <li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li> --}}
                    </ul>
                  </div>
                  <div class="text-center">
                    <h4 class="font-weight-600">&copy;TONGYI GROUP Ltd</h4>
                  </div>
                </div>
              </div>
            </div>
            <!-- SITE Search -->
            <!-- MAIN Vav -->
            <div class="header-nav navbar-collapse collapse">
              <ul class=" nav navbar-nav">
                <li>
                  <a href="{{route('welcome')}}">Home</a>
                </li>
                @foreach ($menus as $menu)
                    <li>
                    <a href="#">{{$menu->name}}</a>
                      <ul class="sub-menu">
                        @foreach ($menu->submenus as $sub)
                            <li>
                              <a href="{{route('menu',['name' =>Str::slug($sub->name,'-')])}}">{{ucwords($sub->name)}}</a>
                            </li>
                        @endforeach
                      </ul>
                    </li>
                @endforeach
              </ul>
            </div>

          </div>
        </div>
      </div>
    </header>
    <!-- HEADER END -->

    <!-- CONTENT START -->
    <div class="page-content">


      <!-- SLIDER START -->
      <div id="welcome_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="goodnews-header"
        data-source="gallery" style="background:#eeeeee;padding:0px;">
        <div id="welcome" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.3.1">
          <ul>
            <!-- SLIDE 1 -->
            <li data-index="rs-901" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
              data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
              data-thumb="{{asset('lib/images/main-slider/slider1/slide1.jpg')}}" data-rotate="0" data-fstransition="fade"
              data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="" data-param1=""
              data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
              data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="{{asset('lib/images/main-slider/slider1/slide1.jpg')}}" alt=""
                data-lazyload="{{asset('lib/images/main-slider/slider1/slide1.jpg')}}" data-bgposition="center center" data-bgfit="cover"
                data-bgparallax="4" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->
              <!-- LAYER NR. 1 [ for overlay ] -->
              <div class="tp-caption tp-shape tp-shapewrapper " id="slide-901-layer-0"
                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"
                data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide"
                data-responsive_offset="off" data-responsive="off" data-frames='[
                            {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                            {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                            ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                style="z-index: 1;background-color:rgba(255, 255, 255, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;">
              </div>




              <!-- LAYER NR. 4 [ for title ] -->
              <div class="tp-caption   tp-resizeme" id="slide-901-layer-2" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['240','240','180','200']" data-fontsize="['72','72','62','52']"
                data-lineheight="['82','82','72','62']" data-width="['700','700','700','90%']"
                data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']"
                data-type="text" data-responsive_offset="on"
                data-frames='[{"delay":"+790","speed":1500,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[0,0,0,0]"
                data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; 
                            white-space: normal; 
                            font-weight: 700;
                            color:#111;
                            border-width:0px;font-family: 'Poppins', sans-serif; text-transform:uppercase ;">
                <div>We Build Your Dream</div>

              </div>

              <!-- LAYER NR. 5 [ for block] -->

              <div class="tp-caption rev-btn  tp-resizeme slider-block" id="slide-901-layer-3"
                data-x="['left','left','left','left']" data-hoffset="['0','80','80','60']"
                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="none"
                data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on"
                data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]' data-textAlign="['left','left','left','left']"
                data-paddingtop="[250,250,250,200]" data-paddingright="[150,150,150,150]"
                data-paddingbottom="[250,250,250,200]" data-paddingleft="[150,150,150,150]" style="z-index: 8;">
                <div class="rs-wave" data-speed="1" data-angle="0" data-radius="2px"></div>
              </div>


              <!-- LAYER NR. 5 [ for block] -->
              <div class="tp-caption   tp-resizeme" id="slide-901-layer-4" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['410','410','350','340']" data-fontsize="['20','20','38','28']"
                data-lineheight="['28','28','48','38']" data-width="['600','600','700','600']"
                data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']"
                data-type="text" data-responsive_offset="on"
                data-frames='[{"delay":"+790","speed":1500,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[0,0,0,0]"
                data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; 
                            white-space: normal; 
                            font-weight: 500;
                            color:#111;
                             border-width:0px;font-family: 'Poppins', sans-serif;">
                Our track record and long list of satisfied customers is unbeatable.

              </div>

              <!-- LAYER NR. 6 [ for see all service botton ] -->
              <div class="tp-caption tp-resizeme" id="slide-901-layer-5" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['500','520','540','480']" data-lineheight="['none','none','none','none']"
                data-width="['300','300','300','300']" data-height="['none','none','none','none']"
                data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on"
                data-frames='[ 
                            {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                            {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                            ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                style="z-index:100; text-transform:uppercase;">
                  <a href="{{route('menu',['name' => 'completed-projects'])}}" class="site-button-secondry btn-effect">More About</a>
              </div>

            </li>


            <!-- SLIDE 2 -->
            <li data-index="rs-902" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
              data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
              data-thumb="lib/images/main-slider/slider1/slide2.jpg" data-rotate="0" data-fstransition="fade"
              data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="" data-param1=""
              data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
              data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="{{asset('lib/images/main-slider/slider1/slide2.jpg')}}" alt=""
                data-lazyload="{{asset('lib/images/main-slider/slider1/slide2.jpg')}}" data-bgposition="center center" data-bgfit="cover"
                data-bgparallax="4" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->
              <!-- LAYER NR. 1 [ for overlay ] -->
              <div class="tp-caption tp-shape tp-shapewrapper " id="slide-902-layer-0"
                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"
                data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide"
                data-responsive_offset="off" data-responsive="off" data-frames='[
                            {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                            {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                            ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                style="z-index: 1;background-color:rgba(255, 255, 255, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;">
              </div>




              <!-- LAYER NR. 4 [ for title ] -->
              <div class="tp-caption   tp-resizeme" id="slide-902-layer-2" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['240','240','180','200']" data-fontsize="['72','72','62','52']"
                data-lineheight="['82','82','72','62']" data-width="['700','700','700','90%']"
                data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']"
                data-type="text" data-responsive_offset="on"
                data-frames='[{"delay":"+790","speed":1500,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[0,0,0,0]"
                data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; 
                            white-space: normal; 
                            font-weight: 700;
                            color:#111;
                            border-width:0px;font-family: 'Poppins', sans-serif; text-transform:uppercase ;">
                <div>From concept to creation.</div>

              </div>

              <!-- LAYER NR. 5 [ for block] -->

              <div class="tp-caption rev-btn  tp-resizeme slider-block" id="slide-902-layer-3"
                data-x="['left','left','left','left']" data-hoffset="['0','80','80','60']"
                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="none"
                data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on"
                data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]' data-textAlign="['left','left','left','left']"
                data-paddingtop="[250,250,250,200]" data-paddingright="[150,150,150,150]"
                data-paddingbottom="[250,250,250,200]" data-paddingleft="[150,150,150,150]" style="z-index: 8;">
                <div class="rs-wave" data-speed="1" data-angle="0" data-radius="2px"></div>
              </div>


              <!-- LAYER NR. 5 [ for block] -->
              <div class="tp-caption   tp-resizeme" id="slide-902-layer-4" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['410','410','350','340']" data-fontsize="['20','20','38','28']"
                data-lineheight="['28','28','48','38']" data-width="['600','600','700','600']"
                data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']"
                data-type="text" data-responsive_offset="on"
                data-frames='[{"delay":"+790","speed":1500,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[0,0,0,0]"
                data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; 
                            white-space: normal; 
                            font-weight: 500;
                            color:#111;
                             border-width:0px;font-family: 'Poppins', sans-serif;">
                We build to perfection and satisfaction.

              </div>

              <!-- LAYER NR. 6 [ for see all service botton ] -->
              <div class="tp-caption tp-resizeme" id="slide-902-layer-5" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['500','520','540','480']" data-lineheight="['none','none','none','none']"
                data-width="['300','300','300','300']" data-height="['none','none','none','none']"
                data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on"
                data-frames='[ 
                            {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                            {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                            ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                style="z-index:100; text-transform:uppercase;">
                <a href="{{route('menu',['name' => 'completed-projects'])}}" class="site-button-secondry btn-effect">More About</a>
              </div>

            </li>


            <!-- SLIDE 3-->
            <li data-index="rs-903" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
              data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
              data-thumb="{{asset('lib/images/main-slider/slider1/slide3.jpg')}}" data-rotate="0" data-fstransition="fade"
              data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="" data-param1=""
              data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
              data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="{{asset('lib/images/main-slider/slider1/slide3.jpg')}}" alt=""
                data-lazyload="{{asset('lib/images/main-slider/slider1/slide3.jpg')}}" data-bgposition="center center" data-bgfit="cover"
                data-bgparallax="4" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->
              <!-- LAYER NR. 1 [ for overlay ] -->
              <div class="tp-caption tp-shape tp-shapewrapper " id="slide-903-layer-0"
                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"
                data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide"
                data-responsive_offset="off" data-responsive="off" data-frames='[
                            {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                            {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                            ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                style="z-index: 1;background-color:rgba(255, 255, 255, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;">
              </div>




              <!-- LAYER NR. 4 [ for title ] -->
              <div class="tp-caption   tp-resizeme" id="slide-903-layer-2" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['240','240','180','200']" data-fontsize="['72','72','62','52']"
                data-lineheight="['82','82','72','62']" data-width="['700','700','700','90%']"
                data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']"
                data-type="text" data-responsive_offset="on"
                data-frames='[{"delay":"+790","speed":1500,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[0,0,0,0]"
                data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; 
                            white-space: normal; 
                            font-weight: 700;
                            color:#111;
                            border-width:0px;font-family: 'Poppins', sans-serif; text-transform:uppercase ;">
                <div>Making dreams come to life</div>

              </div>

              <!-- LAYER NR. 5 [ for block] -->

              <div class="tp-caption rev-btn  tp-resizeme slider-block" id="slide-903-layer-3"
                data-x="['left','left','left','left']" data-hoffset="['0','80','80','60']"
                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="none"
                data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on"
                data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},
                                {"delay":"wait","speed":500,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power1.easeIn"}]' data-textAlign="['left','left','left','left']"
                data-paddingtop="[250,250,250,200]" data-paddingright="[150,150,150,150]"
                data-paddingbottom="[250,250,250,200]" data-paddingleft="[150,150,150,150]" style="z-index: 8;">
                <div class="rs-wave" data-speed="1" data-angle="0" data-radius="2px"></div>
              </div>


              <!-- LAYER NR. 5 [ for block] -->
              <div class="tp-caption   tp-resizeme" id="slide-903-layer-4" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['410','410','350','340']" data-fontsize="['20','20','38','28']"
                data-lineheight="['28','28','48','38']" data-width="['600','600','700','600']"
                data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']"
                data-type="text" data-responsive_offset="on"
                data-frames='[{"delay":"+790","speed":1500,"sfxcolor":"#fff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-textAlign="['left','left','left','left']" data-paddingtop="[5,5,5,5]" data-paddingright="[0,0,0,0]"
                data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13; 
                            white-space: normal; 
                            font-weight: 500;
                            color:#111;
                             border-width:0px;font-family: 'Poppins', sans-serif;">
                Creating unforgetable moments.

              </div>

              <!-- LAYER NR. 6 [ for see all service botton ] -->
              <div class="tp-caption tp-resizeme" id="slide-903-layer-5" data-x="['left','left','left','left']"
                data-hoffset="['50','130','130','130']" data-y="['top','top','top','top']"
                data-voffset="['500','520','540','480']" data-lineheight="['none','none','none','none']"
                data-width="['300','300','300','300']" data-height="['none','none','none','none']"
                data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on"
                data-frames='[ 
                            {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                            {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                            ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                style="z-index:100; text-transform:uppercase;">
                <a href="{{route('menu',['name' => 'completed-projects'])}}" class="site-button-secondry btn-effect">More About</a>
              </div>

            </li>


          </ul>
          <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>

          <div class="multi-line-animation">
            <div class="multi-line-one">
              <div class="animation-block1 bounce-1"></div>
            </div>
            <div class="multi-line-two">
              <div class="animation-block2 bounce-2"></div>
            </div>
            <div class="multi-line-three">
              <div class="animation-block1 bounce-1"></div>
            </div>
            <div class="multi-line-four">
              <div class="animation-block2 bounce-2"></div>
            </div>
          </div>
        </div>



      </div>
      <!-- SLIDER END -->

      <!-- OUR SPECIALLIZATION START -->
      <div class="section-full mobile-page-padding bg-white  p-t80 p-b30 bg-repeat square_shape1"
        style="background-image:url(images/background/bg-5.png);">
        <div class="container">
          <!-- IMAGE CAROUSEL START -->
          <div class="section-content">
            <div class="row">
              <div class="col-lg-3 col-md-4 col-sm-6 m-b30">

                <div class="image-effect-one hover-shadow">
                  <img src="{{ asset('lib/images/pic1.jpg')}}" alt="" />
                  <div class="figcaption">
                    <h4>Construction</h4>
                    <p>Engineer your dreams with us.</p>
                    <a href="javascript:void(0);"><i class="link-plus bg-primary"></i></a>
                  </div>


                </div>

              </div>
              <div class="col-lg-3 col-md-4  col-sm-6 m-b30">

                <div class="image-effect-one hover-shadow">
                  <img src="{{ asset('lib/images/pic2.jpg')}}" alt="" />
                  <div class="figcaption">
                    <h4>Architecture</h4>
                    <p>Life is Architecture.</p>
                    <a href="javascript:void(0);"><i class="link-plus bg-primary"></i></a>
                  </div>

                </div>

              </div>
              <div class="col-lg-3 col-md-4  col-sm-6 m-b30">

                <div class="image-effect-one hover-shadow">
                  <img src="{{ asset('lib/images/pic3.jpg')}}" alt="" />
                  <div class="figcaption bg-dark">
                    <h4>Renovation</h4>
                    <p>We produce dazzling Designs</p>
                    <a href="javascript:void(0);"><i class="link-plus bg-primary"></i></a>
                  </div>

                </div>

              </div>

              <div class="col-lg-3 col-md-12 col-sm-6 m-b30">

                <div class="mt-box our-speciallization-content">
                  <h3 class="m-t0"><span class="font-weight-100">Building</span> <br>Its better in concrete.</h3>
                  <p>When it comes to your house, don’t mess with the rest, trust the best. Making your vision come
                    true, that is what we do.</p>
                  <a href="{{route('menu',['name' => 'completed-projects'])}}" class="site-button btn-effect">View All</a>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="hilite-title text-right p-r50 text-uppercase text-pop-up-top">
          <strong>Welcome</strong>
        </div>
      </div>
      <!-- OUR SPECIALLIZATION END -->

      <!-- ABOUT COMPANY START -->
      <div class="section-full mobile-page-padding p-t80 p-b30 bg-dark bg-repeat square_shape2 bg-moving"
        style="background-image:url(lib/images/background/bg-6.png)">
        <div class="container">
          <!-- TITLE START -->
          <div class="section-head">
            <div class="mt-separator-outer separator-center">
              <div class="mt-separator">
                <h2 class="text-white text-uppercase sep-line-one "><span
                    class="font-weight-300 text-primary">About</span> Company</h2>
              </div>
            </div>
          </div>
          <!-- TITLE END -->

          <div class="section-content">
            <div class="row">
              <div class="col-md-9 col-sm-12">
                <div class="owl-carousel about-home owl-btn-vertical-center">
                  <!-- COLUMNS 1 -->
                  <div class="item ">
                    <div class="mt-img-effect zoom-slow">
                      <a href="javascript:void(0);"><img src="{{asset('lib/images/projects/completed/orange11.jpg')}}" alt=""></a>
                    </div>
                  </div>
                  <!-- COLUMNS 2 -->
                  <div class="item ">
                    <div class="mt-img-effect zoom-slow">
                      <a href="javascript:void(0);"><img src="{{ asset('lib/images/projects/completed/h1.jpeg')}}" alt=""></a>
                    </div>
                  </div>
                  <!-- COLUMNS 3 -->
                  <div class="item ">
                    <div class="mt-img-effect zoom-slow">
                      <a href="javascript:void(0);"><img src="{{asset('lib/images/projects/completed/dolomite1.jpg')}}" alt=""></a>
                    </div>
                  </div>
                  <!-- COLUMNS 4 -->
                  <div class="item ">
                    <div class="mt-img-effect zoom-slow">
                      <a href="javascript:void(0);"><img src="{{('lib/images/projects/completed/shagari1.JPG')}}" alt=""></a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3 col-sm-12">
                <div class="about-home-right bg-white p-a30">
                  <h3 class="m-t0"><span class="font-weight-100">Committed</span> to superior quality and results.</h3>
                  <p><strong>Over the past decade our company has recieved numerous awards
                    <ol>
                      <li>Honorable Contract and Trustworthy Unit</li>
                      <li>Excellent Unit for Security Compliance</li>
                      <li>Top 100 Construction Enterprises in the Province</li>
                      <li>Heavy Safety Demonstration Unit</li>
                      <li>Shenzhen Labor and Social Security Unit</li>
                    </ol>

                  </strong></p>
                  <p>The company successively took part in the construction of over 1,200 kilometers of high-grade highways in more than 20 provinces and autonomous regions in China, built more than 130 large and medium-sized bridges and went abroad to participate in the technical assistance and highway and municipal projects in Cameroon </p>
                  {{-- <div class="text-right">
                    <a href="#" class="site-button-link" data-hover="Read More">Read More <i
                        class="fa fa-angle-right arrow-animation"></i></a>
                  </div> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hilite-title text-left p-l50 text-uppercase hilite-dark">
          <strong>About</strong>
        </div>
      </div>
      <!-- ABOUT COMPANY END -->

      <!-- OUR VALUE START -->
      <div class="section-full mobile-page-padding p-t80 p-b30 bg-white">
        <div class="container">

          <div class="section-content">
            <div class="row">
              <div class="col-md-4 col-sm-12">
                <!-- TITLE START -->
                <div class="section-head">
                  <div class="mt-separator-outer separator-left">
                    <div class="mt-separator">
                      <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Our</span>
                        Values</h2>
                    </div>
                    <p>As a global enterprise engaged in large-scale infrastructure construction and mineral investment business worldwide,
                    Tongyi Group Ltd. has been adhering to the management philosophy of casting fine products and
                    first-class tree brand and actively devotes itself to creating more value for customers.</p>
                  </div>
                </div>
                <!-- TITLE END -->
                <div class="author-info p-t20">
                  <div class="author-name">
                    <h4 class="m-t0">Zheng You</h4>
                    <p>Chairman</p>
                  </div>
                </div>
                <div class="video-section-full bg-no-repeat bg-cover overlay-wraper"
                  style="background-image:url(lib/images/video-bg.jpg)">
                  <div class="overlay-main bg-black opacity-07"></div>
                  <div class="video-section-inner">
                    <div class="video-section-content">
                      <div class="video-section-left">
                        <a href="https://player.vimeo.com/video/34741214?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0"
                          class="mfp-video play-now">
                          <i class="icon fa fa-play"></i>
                          <span class="ripple"></span>
                        </a>
                      </div>
                      <div class="video-section-right">
                        <a href="https://player.vimeo.com/video/34741214?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0"
                          class="mfp-video font-weight-600 text-uppercase">Video Presentation</a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-3 col-sm-6">
              
                <div class="mt-count m-b30 text-white mt-icon-box-wraper center  bg-dark p-a20">
                  <div class="counter font-30 font-weight-800 m-b15 text-primary">40</div>
                  <h4 class="m-tb0">Active Experts</h4>
                </div>
                <div class="mt-count m-b30 text-white mt-icon-box-wraper center  bg-dark p-a20">
                  <div class="counter font-30 font-weight-800 m-b15  text-primary">670</div>
                  <h4 class="m-tb0">Satisfied Clients</h4>
                </div>
                <div class="mt-count m-b30 text-white mt-icon-box-wraper center  bg-dark p-a20">
                  <div class="counter font-30 font-weight-800 m-b15 text-primary">1500</div>
                  <h4 class="m-tb0">Projects Completed</h4>
                </div>
              </div>
              <div class="col-md-5 col-sm-6">
                <div class="p-b0">
                  <div class="mt-box">
                    <h3 class="m-t0"><span class="font-weight-100"> We have</span><span class="text-primary">&nbsp;over 20 
                        years</span> experience in construction</h3>
                  </div>
                  <span class="progressText text-black"><B>Architecture</B></span>
                  <div class="progress mt-probar-1  m-b30 m-t10">
                    <div class="progress-bar bg-primary " role="progressbar" aria-valuenow="85" aria-valuemin="0"
                      aria-valuemax="100">
                      <span class="popOver" data-toggle="tooltips" data-placement="top" title="85%"></span>
                    </div>
                  </div>

                  <span class="progressText text-black"><B>Road Construction</B></span>
                  <div class="progress mt-probar-1 m-b30 m-t10">
                    <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="78" aria-valuemin="10"
                      aria-valuemax="100">
                      <span class="popOver" data-toggle="tooltips" data-placement="top" title="78%"></span>
                    </div>
                  </div>

                  <span class="progressText text-black"><B>Bridges Construction</B></span>
                  <div class="progress mt-probar-1 m-b30 m-t10">
                    <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="65" aria-valuemin="0"
                      aria-valuemax="100">
                      <span class="popOver" data-toggle="tooltips" data-placement="top" title="65%"></span>
                    </div>
                  </div>

                  <span class="progressText text-black"><B>Tunnel Construction</B></span>
                  <div class="progress mt-probar-1 m-b30 m-t10">
                    <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0"
                      aria-valuemax="100">
                      <span class="popOver" data-toggle="tooltips" data-placement="top" title="40%"></span>
                    </div>
                  </div>

                  {{-- <span class="progressText text-black"><B>Traffic Engineering</B></span>
                  <div class="progress mt-probar-1 m-b30 m-t10">
                    <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="55" aria-valuemin="0"
                      aria-valuemax="100">
                      <span class="popOver" data-toggle="tooltips" data-placement="top" title="55%"></span>
                    </div>
                  </div> --}}

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- OUR VALUE END -->

      <!-- OUR MISSION START -->
      <div
        class="section-full mobile-page-padding mission-outer-section  p-t80 p-b30 bg-gray bg-no-repeat bg-right-center"
        style="background-image:url(lib/images/left-men.png), url(lib/images/background/bg-4.png); ">
        <div class="section-content">
          <div class="container">
            <!-- TITLE START -->
            <div class="section-head">
              <div class="mt-separator-outer separator-center">
                <div class="mt-separator">
                  <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Our</span> Mission
                  </h2>
                </div>
              </div>
            </div>
            <!-- TITLE END -->

            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="mission-left bg-white m-b30 p-a30 bg-no-repeat bg-bottom-left"
                  style="background-image:url(lib/images/background/bg-site.png);">
                  <h3 class="m-t0"><span class="font-weight-100"> Our</span><br>Core Competence</h3>
                  <p>Over the years we have undertaken various projects and have developed strong competencies in the following areas</p>
                  <ul class="list-angle-right anchor-line">
                    <li><a href="#">Road Construction</a></li>
                    <li><a href="#">Bridge Construction</a></li>
                    <li><a href="#">Mining Engineering</a></li>
                    <li><a href="#">Tunnel Construction</a></li>
                    <li><a href="#">Building Construction</a></li>
                  </ul><br><br>
                  {{-- <div class="text-right">
                    <a href="#" class="site-button-link" data-hover="Read More">Read More <i
                        class="fa fa-angle-right arrow-animation"></i></a>
                  </div> --}}
                </div>
              </div>

              <div class="col-md-4 col-sm-6">
                <div class="mission-mid bg-no-repeat bg-cover m-b30" style="background-image:url(lib/images/mission.jpg);">
                </div>
              </div>

              <div class="col-md-4 col-sm-12">
                <div class="contact-home1-left bg-dark p-a30 m-b0">
                  <h3 class="text-white m-t0"><span class="font-weight-100">Get In</span> Touch</h3>
                  <form class="contact-form cons-contact-form" method="post" action="{{route('enquiry')}}" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="input input-animate">
                      <label for="name">Name</label>
                      <input type="text" name="name" id="name" required>
                      <span class="spin"></span>
                    </div>

                    <div class="input input-animate">
                      <label for="email">Email</label>
                      <input type="email" name="email" id="email" required>
                      <span class="spin"></span>
                    </div>

                    <div class="input input-animate">
                      <label for="Phone">Phone</label>
                      <input type="text" name="phone" id="Phone" required>
                      <span class="spin"></span>
                    </div>

                    <div class="input input-animate">
                      <label for="message">Message</label>
                      <textarea name="message" id="message" required></textarea>
                      <span class="spin"></span>
                    </div>

                    <div class="text-center p-t10">
                      <button type="submit" class="site-button btn-effect ">
                        Submit Now
                      </button>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="hilite-title text-left p-l50 text-uppercase text-pop-up-top">
          <strong>Mission</strong>
        </div>
      </div>
      <!-- OUR MISSION  END -->

      <!-- OUR SERVICES START -->
      <div class="section-full mobile-page-padding  p-b50  square_shape2">
        <div class="section-content">
          <div class="Service-half-top p-t80  bg-dark bg-moving"
            style="background-image:url(lib/images/background/bg-6.png)">
            <div class="container">
              <!-- TITLE START -->
              <div class="section-head text-white">
                <div class="mt-separator-outer separator-left">
                  <div class="mt-separator">
                    <h2 class="text-white text-uppercase sep-line-one "><span
                        class="font-weight-300 text-primary">Our</span> Services</h2>
                  </div>
                </div>
              </div>
              <!-- TITLE END -->
            </div>
          </div>
          <div class="services-half-bottom">
            <div class="container">
              <div class="row">

                <div class="col-md-4 col-sm-6">

                  <div class="mt-icon-box-wraper m-b30">
                    <div class="relative icon-count-2 bg-gray p-a30 p-tb50">
                      <span class="icon-count-number">1</span>
                      <div class="icon-md inline-icon m-b15 text-primary scale-in-center">
                        <span class="icon-cell"><img src="{{ asset('lib/images/icon/renovation.png')}}" alt=""></span>
                      </div>
                      <div class="icon-content">
                        <h4 class="mt-tilte m-b25">Highway <br>Construction</h4>
                        <p>We construct roads that stand the test of time. Your road project is better handled by us. Talk to us</p><br>
                      <a href="{{ route('menu',['name' => 'highways'])}}" class="site-button-link" data-hover="Read More">Read More <i
                            class="fa fa-angle-right arrow-animation"></i></a>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="col-md-4 col-sm-6">

                  <div class="mt-icon-box-wraper m-b30">
                    <div class="relative icon-count-2 bg-gray p-a30 p-tb50">
                      <span class="icon-count-number">2</span>
                      <div class="icon-md inline-icon m-b15 text-primary scale-in-center">
                        <span class="icon-cell"><img src="{{asset('lib/images/icon/toolbox.png')}}" alt=""></span>
                      </div>
                      <div class="icon-content">
                        <h4 class="mt-tilte m-b25">Bridge <br>Construction</h4>
                        <p>Our bridges are contructed with the best quality materials available to ensure durability, maintainability and endure hash weather </p>
                      <a href="{{route('menu',['name' => 'bridges'])}}" class="site-button-link" data-hover="Read More">Read More <i
                            class="fa fa-angle-right arrow-animation"></i></a>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="col-md-4 col-sm-6">

                  <div class="mt-icon-box-wraper m-b30">
                    <div class="relative icon-count-2 bg-gray p-a30 p-tb50">
                      <span class="icon-count-number">3</span>
                      <div class="icon-md inline-icon m-b15 text-primary scale-in-center">
                        <span class="icon-cell"><img src="{{ asset('lib/images/icon/compass.png')}}" alt=""></span>
                      </div>
                      <div class="icon-content">
                        <h4 class="mt-tilte m-b25">Mining<br> Engineering</h4>
                        <p>We are fully equipped to undertake all levels of mining operations. From extraction to processing to packaging of the finished products.</p>
                        <a href="{{route('menu',['name' => 'mining'])}}" class="site-button-link" data-hover="Read More">Read More <i
                            class="fa fa-angle-right arrow-animation"></i></a>
                      </div>
                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="hilite-title text-left p-l50 text-uppercase text-pop-up-top">
          <strong>Services</strong>
        </div>
      </div>
      <!-- OUR SERVICES  END -->

      <!-- CALL US SECTION START -->
      <div class="section-full mobile-page-padding p-tb80 overlay-wraper bg-cover bg-center"
        style="background-image:url(lib/images/background/bg-1.jpg)">
        <div class="overlay-main bg-primary opacity-07"></div>
        <div class="container">

          <div class="section-content">
            <div class="call-us-section text-center">
              <h4 class="m-b15">We are here to serve You</h4>
              <h2 class="call-us-number m-b15 m-b0">Call Now<br> Mrs Jiang +2348171966666</h2>
              {{-- <h4 class="call-us-address m-t0 m-b20">Plot 105，Ebitu Ukiwe Street，Jabi，Abuja，Nigeria</h4> --}}
            <a href="{{route('menu',['name' => 'contact-us'])}}" class="site-button-secondry btn-effect bg-dark">Book An Appointment</a>
            </div>
          </div>

        </div>
      </div>
      <!-- CALL US SECTION END -->

      <!-- OUR PROJECT START -->
      <div class="section-full mobile-page-padding p-t80 p-b30 square_shape2">
        <div class="container">
          <!-- TITLE START -->
          <div class="section-head">
            <div class="mt-separator-outer separator-left">
              <div class="mt-separator">
                <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Our</span> Project
                </h2>
              </div>
            </div>
          </div>
          <!-- TITLE END -->

          <div class="section-content">

            <div class="portfolio-wrap row mfp-gallery product-stamp clearfix">
              <!-- COLUMNS 1 -->
              <div class="stamp col-md-3 col-sm-6 m-b30">
                <div class="bg-gray p-a30">
                  <div class="filter-wrap">
                    <ul class="filter-navigation masonry-filter text-uppercase">
                      <li class="active"><a data-filter="*" data-hover="All" href="#">All</a></li>
                      <li><a data-filter=".cat-1" data-hover="High Rise Buildings" href="javascript:;">High Rise Buildings</a></li>
                      <li><a data-filter=".cat-4" data-hover="Tunnel Construction" href="javascript:;">Tunnel Construction</a></li>
                      <li><a data-filter=".cat-3" data-hover="Residential" href="javascript:;">Residential</a></li>
                      <li><a data-filter=".cat-5" data-hover="Maintainance" href="javascript:;">Maintainance</a></li>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- COLUMNS 2 -->
              <div class="masonry-item  cat-2 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{asset('lib/images/projects/portrait/pic1.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">Bridge Construction</h4>
                    <p class="m-b0"></p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>

              <!-- COLUMNS 3 -->
              <div class="masonry-item  cat-1 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{asset('lib/images/projects/portrait/pic2.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">High Rise</h4>
                    <p class="m-b0">Building construction</p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>
              <!-- COLUMNS 4 -->
              <div class="masonry-item  cat-1 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{ asset('lib/images/projects/portrait/pic3.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">Building</h4>
                    <p class="m-b0">Glass house</p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>
              <!-- COLUMNS 5 -->
              <div class="masonry-item  cat-3 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{ asset('lib/images/projects/portrait/pic4.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">Residential building</h4>
                    <p class="m-b0">Dream Workspace</p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>
              <!-- COLUMNS 6 -->
              <div class="masonry-item  cat-1 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{('lib/images/projects/portrait/pic5.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">High Rise</h4>
                    <p class="m-b0">High rise cladding</p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>

              <!-- COLUMNS 7 -->
              <div class="masonry-item  cat-4 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{('lib/images/projects/portrait/pic6.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">Tunnel construction</h4>
                    <p class="m-b0">Heavy earth equitment</p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>

              <!-- COLUMNS 8 -->
              <div class="masonry-item  cat-3 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{('lib/images/projects/portrait/pic7.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">Residential building</h4>
                    <p class="m-b0">North House</p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>

              <!-- COLUMNS 11 -->
              <div class="masonry-item  cat-5 col-md-6 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{('lib/images/projects/pic-l-5.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">Maintainance</h4>
                    <p class="m-b0">Maintaining a high rise building</p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>
              <!-- COLUMNS 9 -->
              <div class="masonry-item  cat-4 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{('lib/images/projects/pic-1.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">Tunnel Construction</h4>
                    <p class="m-b0">Heavy Earth tunneling</p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>

              <!-- COLUMNS 10 -->
              <div class="masonry-item  cat-1 col-md-3 col-sm-6 m-b30">
                <div class="mt-box   image-hover-block">
                  <div class="mt-thum-bx">
                    <img src="{{('lib/images/projects/pic-2.jpg')}}" alt="">
                  </div>
                  <div class="mt-info  p-t20 text-white">
                    <h4 class="mt-tilte m-b10 m-t0">Building Construction</h4>
                    <p class="m-b0">Our Architecte with drawings </p>
                  </div>
                  <a href="#"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hilite-title text-left p-l50 text-uppercase text-pop-up-top">
          <strong>Projects</strong>
        </div>
      </div>
      <!-- OUR PROJECT END -->

                   <!-- OUR BLOG START -->
<div class="section-full mobile-page-padding p-t80 p-b30 square_shape1 bg-parallax bg-cover"  data-stellar-background-ratio="0.5" style="background-image:url({{asset('lib/images/background/bg5.jpg')}});">
  <div class="container">
      <!-- TITLE START -->
    <div class="section-head ">
        <div class="mt-separator-outer separator-center">
            <div class="mt-separator">
                <h2 class="text-white text-uppercase sep-line-one "><span class="font-weight-300 text-primary" >Latest </span> News</h2>
            </div>
        </div>
    </div>                   
<!-- TITLE END --> 
             
<!-- IMAGE CAROUSEL START -->
  <div class="section-content">
        <div class="row">
          @foreach ($news as $newsItem)
          <div class="col-md-4 col-sm-6">
                <div class="mt-box blog-post latest-blog-3 date-style-1 bg-white m-b30">
                    <div class="mt-post-media mt-img-overlay7">
                    <a href="javascript:;"><img src="{{ asset($newsItem->image)}}" alt=""></a>
                    </div>
                    <div class="mt-post-info p-a30">
                        <div class="post-overlay-position">

                            <div class="mt-post-meta ">
                                <ul>
                                <li class="post-date"><strong class="text-primary">{{$newsItem->created_at->format('d')}}</strong> {{$newsItem->created_at->format('M')}} <span>{{$newsItem->created_at->format('Y')}}  <i>{{$newsItem->created_at->diffForHumans()}}</i></span></li>
                                    <li class="post-author">By <a href="#"> Admin</a> </li>
                                </ul>
                            </div>                                        
                            <div class="mt-post-title ">
                            <h4 class="post-title m-b0">{{$newsItem->title}}</h4>
                            </div>
                            <div class="mt-post-text">
                            <p>{{substr($newsItem->content,0,100)}}</p> 
                            </div>
                            <div class="readmore-line">
                                <span><a href="{{route('readmore',['slug' => $newsItem->slug])}}" class="site-button-link" data-hover="Read More">Read More <i class="fa fa-angle-right arrow-animation"></i></a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
          @endforeach                                                                                                                                                                                       
        </div>
    </div>
  </div>
<div class="hilite-title text-right p-r50 text-uppercase hilite-dark">
    <strong>News</strong>
</div>                
</div>   
<!-- OUR BLOG END -->

      <!-- TESTIMONIALS SECTION START -->
      <div class="section-full mobile-page-padding p-t80 p-b50 square_shape2 bg-cover"
        style="background-image:url({{asset('lib/images/background/bg6.jpg')}});">
        <div class="container">
          <div class="section-content">

            <!-- TITLE START -->
            <div class="section-head">
              <div class="mt-separator-outer separator-center">
                <div class="mt-separator">
                  <h2 class="text-uppercase sep-line-one "><span class="font-weight-300 text-primary">Client</span>
                    Testimonials</h2>
                </div>
              </div>
            </div>
            <!-- TITLE END -->

            <!-- TESTIMONIAL START -->
            <div class="owl-carousel testimonial-home">
              <div class="item">
                <div class="testimonial-2 m-a30 hover-animation-1">
                  <div class=" block-shadow bg-white p-a30">
                    <div class="testimonial-detail clearfix">
                      <div class="testimonial-pic radius shadow scale-in-center"><img src="{{('lib/images/testimonials/pic1.jpg')}}"
                          width="100" height="100" alt=""></div>
                      <h4 class="testimonial-name m-b5">Shelly Johnson -</h4>
                      <span class="testimonial-position">Executive Assitant to the Governor</span>
                    </div>
                    <div class="testimonial-text">
                      <span class="fa fa-quote-right"></span>
                      <p> The road you constructed is really smooth and very strong, the finishing of the drainages is beautiful. The governor is highly pleased and the people are jubilating.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-2 m-a30  hover-animation-1">
                  <div class=" block-shadow bg-white p-a30">
                    <div class="testimonial-detail clearfix">
                      <div class="testimonial-pic radius shadow scale-in-center"><img src="{{('lib/images/testimonials/pic2.jpg')}}"
                          width="100" height="100" alt=""></div>
                      <h4 class="testimonial-name m-b5"> Brain James -</h4>
                      <span class="testimonial-position">Contractor</span>
                    </div>
                    <div class="testimonial-text">
                      <span class="fa fa-quote-right"></span>
                      <p>You actually exceeded our expectations. We were told not to uses your company but we took a chance with you and you exceeded our expectations by your exceptional performance.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-2 m-a30  hover-animation-1">
                  <div class=" block-shadow bg-white p-a30">
                    <div class="testimonial-detail clearfix">
                      <div class="testimonial-pic radius shadow scale-in-center"><img src="{{asset('images/testimonials/pic3.jpg')}}"
                          width="100" height="100" alt=""></div>
                      <h4 class="testimonial-name m-b5">Cathrine Wagner -</h4>
                      <span class="testimonial-position">Builder</span>
                    </div>
                    <div class="testimonial-text">
                      <span class="fa fa-quote-right"></span>
                      <p>The finishing of the building is what is most inriguing about the whole project, its simply awesome. Your attention to detail is really surprising for a company this large.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-2 m-a30  hover-animation-1">
                  <div class=" block-shadow bg-white p-a30">
                    <div class="testimonial-detail clearfix">
                      <div class="testimonial-pic radius shadow scale-in-center"><img src="{{('lib/images/testimonials/pic4.jpg')}}"
                          width="100" height="100" alt=""></div>
                      <h4 class="testimonial-name m-b5">John Smith -</h4>
                      <span class="testimonial-position">Business Person</span>
                    </div>
                    <div class="testimonial-text">
                      <span class="fa fa-quote-right"></span>
                      <p> Thank you so much for delivering the project on time</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-2 m-a30  hover-animation-1">
                  <div class=" block-shadow bg-white p-a30">
                    <div class="testimonial-detail clearfix">
                      <div class="testimonial-pic radius shadow scale-in-center"><img src="{{('lib/images/testimonials/pic5.jpg')}}"
                          width="100" height="100" alt=""></div>
                      <h4 class="testimonial-name m-b5">Carl Brain -</h4>
                      <span class="testimonial-position">Business Person</span>
                    </div>
                    <div class="testimonial-text">
                      <span class="fa fa-quote-right"></span>
                      <p>The office complex is awesome. You gave us mor than we bargained for. Thank you so much.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hilite-title text-left p-l50 text-uppercase text-pop-up-top">
          <strong>Clients</strong>
        </div>
      </div>
      <!-- TESTIMONIALS SECTION END -->
    </div>
    <!-- CONTENT END -->

    <!-- FOOTER START -->
    <footer class="site-footer footer-large  footer-dark	footer-wide">
      <div class="container call-to-action-wrap bg-no-repeat bg-center"
        style="background-image:url(lib/images/background/bg-site.png);">
        <div class="p-a30 bg-primary ">
          <p style="font-size:18px;line-height:30px;text-align:center">We let our quality work and commitment to customer satisfaction be our slogan.<br> <span style="text-align: center">Quality you deserve and dependability you can count on.</span><br> <span style="text-align: center">Call Now Mrs Jiang +2348171966666</span></p>

        </div>
      </div>
      <!-- FOOTER BLOCKES START -->
      <div class="footer-top overlay-wraper">
        <div class="overlay-main"></div>
        <div class="container">
          <div class="row">
            <!-- ABOUT COMPANY -->
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="widget widget_about">
                <!--<h4 class="widget-title">About Company</h4>-->
                <div class="logo-footer clearfix p-b15">
                <a href="{{route('welcome')}}"><img src="{{asset('lib/images/bgoff.jpg')}}" alt="" style="height:60px">
                </a>
                <span style="margin-left:;font-size:20px">TONGYI GROUP LTD</span>
                </div>
                <p class="max-w400">Today we can tell you, thanks to your passion, hard work creativity, and expertise,
                  you delivered us the most beautiful house great looks.</p>

                <ul class="social-icons  mt-social-links">
                  <li><a href="javascript:void(0);" class="fa fa-google"></a></li>
                  <li><a href="javascript:void(0);" class="fa fa-rss"></a></li>
                  <li><a href="javascript:void(0);" class="fa fa-facebook"></a></li>
                  <li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
                  <li><a href="javascript:void(0);" class="fa fa-linkedin"></a></li>
                </ul>
              </div>
            </div>

            <!-- RESENT POST -->
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="widget widget_address_outer">
                <h4 class="widget-title">Contact Us</h4>
                <ul class="widget_address">
                  <li>Plot 105，Ebitu Ukiwe Street，Jabi，Abuja，Nigeria</li>
                  <li>+2348171966666</li>
                </ul>

              </div>
            </div>

            <!-- USEFUL LINKS -->
            <div class="col-lg-3 col-md-6 col-sm-6 footer-col-3">
              <div class="widget widget_services inline-links">
                <h4 class="widget-title">Useful links</h4>
                <ul>
                  <li><a href="{{route('menu',['name' => 'group-introduction'])}}">About</a></li>
                  <li><a href="{{route('menu',['name' => 'completed-projects'])}}">Projects</a></li>
                  <li><a href="{{route('menu',['name' => 'group-news'])}}">News</a></li>
                <li><a href="{{route('menu',['name' => 'contact-us'])}}">Contact Us</a></li>
                </ul>
              </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="widget recent-posts-entry-date">
                <h4 class="widget-title">Recent News</h4>
                <div class="widget-post-bx">

                  @foreach ($footerNews as $fn)
                    <div class="bdr-light-blue widget-post clearfix  bdr-b-1 m-b10 p-b10">
                    <div class="mt-post-date text-center text-uppercase text-white p-tb5">
                    <strong class="p-date">{{$fn->created_at->format('d')}}</strong>
                    <span class="p-month">{{$fn->created_at->format('M')}}</span>
                    <span class="p-year">{{$fn->created_at->format('Y')}}</span>
                    </div>
                    <div class="mt-post-info">
                      <div class="mt-post-header">
                        <h6 class="post-title"><a href="{{route('readmore',['slug' => $fn->slug])}}">{{$fn->title}}</a>
                        </h6>
                      </div>
                      <div class="mt-post-meta">
                        <ul>
                          <li class="post-author"><i class="fa fa-user"></i>By Admin</li>
                        </ul>
                      </div>
                    </div>
                  </div>  
                  @endforeach

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- FOOTER COPYRIGHT -->
      <div class="footer-bottom overlay-wraper">
        <div class="overlay-main"></div>
        <div class="container">
          <div class="row">
            <div class="mt-footer-bot-center">
              <span class="copyrights-text">© 2019 All rights reserved </span>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- FOOTER END -->

    <!-- BUTTON TOP START -->
    <button class="scroltop"><span class="fa fa-angle-up  relative" id="btn-vibrate"></span></button>

  </div>

  <!-- LOADING AREA START ===== -->
  {{-- <div class="loading-area">
    <div class="loading-box"></div>
    <div class="loading-pic">
      <div class="cssload-loader">Loading</div>
    <img src="{{asset('lib/images/preloader.gif')}}" />
    </div>
  </div> --}}
  <!-- LOADING AREA  END ====== -->

  <!-- JAVASCRIPT  FILES ========================================= -->
  <script src="{{ asset('lib/js/jquery-1.12.4.min.js')}}"></script><!-- JQUERY.MIN JS -->
  <script src="{{ asset('lib/js/bootstrap.min.js')}}"></script><!-- BOOTSTRAP.MIN JS -->

  <script src="{{ asset('lib/js/magnific-popup.min.js')}}"></script><!-- MAGNIFIC-POPUP JS -->

  <script src="{{ asset('lib/js/waypoints.min.js')}}"></script><!-- WAYPOINTS JS -->
  <script src="{{ asset('lib/js/counterup.min.js')}}"></script><!-- COUNTERUP JS -->
  <script src="{{ asset('lib/js/waypoints-sticky.min.js')}}"></script><!-- COUNTERUP JS -->

  <script src="{{ asset('lib/js/isotope.pkgd.min.js')}}"></script><!-- MASONRY  -->

  <script src="{{ asset('lib/js/owl.carousel.min.js')}}"></script><!-- OWL  SLIDER  -->
  <script src="{{ asset('lib/js/jquery.owl-filter.js')}}"></script>

  <script src="{{ asset('lib/js/stellar.min.js')}}"></script><!-- PARALLAX BG IMAGE   -->



  <script src="{{ asset('lib/js/custom.js')}}"></script><!-- CUSTOM FUCTIONS  -->
  <script src="{{ asset('lib/js/shortcode.js')}}"></script><!-- SHORTCODE FUCTIONS  -->
  <script src="{{ asset('lib/js/jquery.bgscroll.js')}}"></script><!-- BACKGROUND SCROLL -->

  <!-- REVOLUTION JS FILES -->

  <script src="{{ asset('lib/plugins/revolution/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
  <script src="{{ asset('lib/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>

  <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
  <script src="{{ asset('lib/plugins/revolution/revolution/js/extensions/revolution-plugin.js')}}"></script>

  <!-- REVOLUTION SLIDER SCRIPT FILES -->
  <script src="{{ asset('lib/js/rev-script-1.js')}}"></script>



</body>

</html>