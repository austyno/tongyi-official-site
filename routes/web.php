<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Admin
Route::group(['prefix'=> 'admin'], function(){

    //news
    Route::get('news/all',[
        'uses'  => 'NewsController@index',
        'as'    => 'news'
    ]);
    Route::get('news/toggle/published/{status}/{id}',[
        'uses'  =>  'NewsController@togglePublished',
        'as'    =>  'news.published.toggle'
    ]);
    Route::get('/news/create',[
        'uses'  =>  'NewsController@create',
        'as'    =>  'news.create'
    ]);
    Route::post('news/store',[
        'uses'  =>  'NewsController@store',
        'as'    =>  'news.store'
    ]);
    Route::get('news/edit/{id}',[
        'uses'  =>  'NewsController@edit',
        'as'    =>  'news.edit'
    ]);
    Route::post('news/edit/{id}',[
        'uses'  =>  'NewsController@update',
        'as'    =>  'news.update'
    ]);
    Route::get('news/view/{slug}',[
        'uses'  =>  'NewsController@show',
        'as'    =>  'news.show'
    ]);

    //ongoing projects
    Route::get('projects/ongoing',[
        'uses'  =>  'OngoingProjects@index',
        'as'    =>  'ongoing.all'
    ]);
    Route::get('ongoing/{id}',[
        'uses'  =>  'OngoingProjects@viewImages',
        'as'    =>  'ongoing.images'
    ]);
    Route::get('projects/ongoing/new-project',[
        'uses'  =>  'OngoingProjects@create',
        'as'    =>  'ongoing.newProject'
    ]);
    Route::post('projects/ongoing/new-project',[
        'uses'  =>  'OngoingProjects@store',
        'as'    =>  'ongoing.store'
    ]);
    Route::post('projects/ongoing/upload/{id}/{status}',[
        'uses'  =>  'OngoingProjects@imageUpld',
        'as'    =>  'ongoing.upload'
    ]);
    Route::get('ongoing/new-image/{proId}/{status}/{name}',[
        'uses'  =>  'OngoingProjects@newImages',
        'as'    =>  'ongoing.newImages'
    ]);
    Route::get('/ongoing/edit/{id}',[
        'uses'  => 'OngoingProjects@edit',
        'as'    =>  'ongoing.edit' 
    ]);
    Route::post('/ongoing/edit/{id}',[
        'uses'  => 'OngoingProjects@update',
        'as'    =>  'ongoing.update' 
    ]);
    Route::get('/ongoing/del/{id}',[
        'uses'  => 'OngoingProjects@deleteOngoingProject',
        'as'    =>  'ongoing.del'
    ]);
    

    //completed projects
    Route::get('projects/completed',[
        'uses'  =>  'CompletedProjects@index',
        'as'    =>  'completed.all'
    ]);
    Route::get('projects/completed/new-project',[
        'uses'  => 'CompletedProjects@create',
        'as'    =>  'completed.newProject'
    ]);
    Route::post('projects/completed/new-project',[
        'uses'  => 'CompletedProjects@store',
        'as'    =>  'completed.store'
    ]);
    Route::get('completed/{id}',[
        'uses'  =>  'CompletedProjects@viewImages',
        'as'    =>   'completed.images'
    ]);
    Route::get('completed/new-image/{proId}/{status}/{name}',[
        'uses'  =>  'CompletedProjects@newImages',
        'as'    =>  'completed.newImages'
    ]);
    Route::post('completed/upload/{id}/{status}',[
        'uses'  =>  'CompletedProjects@imageUpld',
        'as'    =>  'completed.upload'
    ]);
    Route::get('/completed/edit/{id}',[
        'uses'  => 'CompletedProjects@edit',
        'as'    =>  'completed.edit' 
    ]);
    Route::post('/completed/edit/{id}',[
        'uses'  => 'OngoingProjects@update',
        'as'    =>  'completed.update' 
    ]);
    Route::get('/completed/del/{id}',[
        'uses'  => 'CompletedProjects@deleteOngoingProject',
        'as'    =>  'completed.del'
    ]);

    //vacancy
    Route::get('vacancy/all',[
        'uses'  => 'VacancyController@index',
        'as'    => 'jobs.all'
    ]);
    Route::get('vacancy/new',[
        'uses'  => 'VacancyController@create',
        'as'    =>  'jobs.create'
    ]);
    Route::post('vacancy/store',[
        'uses'  =>  'VacancyController@store',
        'as'    =>  'jobs.store'
    ]);
    Route::get('vacancy/delete/{id}',[
        'uses'  =>   'VacancyController@destroy',
        'as'    =>    'jobs.delete'
    ]);
    Route::get('vacancy/edit/{id}',[
        'uses'  => 'VacancyController@edit',
        'as'    =>  'jobs.edit'
    ]);
    Route::post('vacancy/update/{id}',[
        'uses'  => 'VacancyController@update',
        'as'    =>  'jobs.update'
    ]);
    
    //applications
    Route::get('applications/all',[
        'uses'  =>  'VacancyController@applications',
        'as'    =>  'applications.all'
    ]);
    Route::get('resume/{id}',[
        'uses'  =>  'VacancyController@show',
        'as'    =>   'resume'
    ]);
    Route::get('delete/application/{id}',[
        'uses'  =>  'VacancyController@deleteApplication',
        'as'    =>  'application.delete'
    ]);

    //enquiries
    Route::get('enquiries',[
        'uses'  =>  'EnquiryController@index',
        'as'    =>  'enquire.all'
    ]);
    Route::get('delete/enquiry/{id}',[
        'uses'  =>  'EnquiryController@destroy',
        'as'    =>  'enquire.del'
    ]);

});

//ajax
    Route::post('/delete',[
        'uses'  =>  'NewsController@destroy',
        'as'    =>  'news.del'
    ]);
    Route::post('/deleteImg',[
        'uses'  => 'OngoingProjects@destroy',
        'as'    =>  'deleteImg' 
    ]);
    Route::post('/letter',[
        'uses'  =>  'VacancyController@CoverLetter',
        'as'    =>  'letter'
    ]);
    Route::get('/messages',[
        'uses'  =>  'EnquiryController@message',
        'as'    =>  'messages'
    ]);
    

//frontend
Route::get('/',[
    'uses' => 'PagesController@index',
    'as' => 'welcome'
]);
Route::get('/page/{name}',[
    'uses' => 'PagesController@menuPages',
    'as' => 'menu'
]);
Route::get('/readmore/{slug}',[
    'uses' => 'PagesController@readmore',
    'as' => 'readmore'
]);
Route::get('/viewimages/{status}/{slug}',[
    'uses' => 'PagesController@viewimages',
    'as' => 'viewimages'
]);
Route::get('/download',[
    'uses' => 'PagesController@getDownload',
    'as' => 'download'
]);

Route::post('apply',[
    'uses'  =>  'PagesController@apply',
    'as'    =>  'apply'
]);
Route::post('/enquiry',[
    'uses'  =>  'PagesController@enquiry',
    'as'    =>  'enquiry'
]);


